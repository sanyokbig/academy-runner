import 'babel-polyfill';

import fs from 'fs';
import webpack from 'webpack';
import path from 'path';

const nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function (x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function (mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

const projectRootPath = path.resolve(__dirname, '../'),
    assetsPath = path.resolve(projectRootPath, './build'),
    jsName = 'server.js';

module.exports = {
    devtool: 'inline-source-map',
    target: 'node',
    node: {
        __dirname: false,
        __filename: false
    },
    context: path.resolve(__dirname, '..'),
    entry: {
        bundle: [
            'babel-polyfill',
            './src/server.js'
        ]
    },
    externals: nodeModules,
    output: {
        path: assetsPath,
        publicPath: assetsPath,
        filename: jsName,
        libraryTarget: 'commonjs2'
    },
    resolve: {
        modules: ['src', 'node_modules'],
        extensions: ['.js', '.jsx', '.styl', '.yaml']
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            loader: 'babel-loader'
        },{
            test: /\.yaml$/,
            exclude: /node_modules/,
            loader: 'file-loader',
            options: {
                name: 'translations/[name].[ext]'
            }
        }]
    },
    plugins: [
        // Чистка директории
        new webpack.NormalModuleReplacementPlugin(/\.(css|styl|jpg|ico)$/, 'node-noop'),
        new webpack.IgnorePlugin(/\.(css|styl|jpg|ico)$/),
        // Глобальные константы
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"',
                PROD: true,
                BROWSER: false,
                PORT: process.env.PORT
            }
        }),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.LoaderOptionsPlugin({
            debug: true,
            minimize: true,
            options: {
                progress: true
            }
        })
    ]
};