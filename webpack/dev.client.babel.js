import 'babel-polyfill';

import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import ManifestPlugin from 'webpack-manifest-plugin';

const projectRootPath = path.resolve(__dirname, '../'),
    assetsPath = path.resolve(projectRootPath, './static/dist'),
    jsName = '[name].js',
    jsChunkName = '[name].js',
    cssName = '[name].css',
    host = (process.env.HOST) || 'localhost',
    port = (+process.env.PORT + 1) || 3001,
    publicPath = 'http://' + host + ':' + port + '/dist/';

module.exports = {
    devtool: 'eval',
    context: path.resolve(__dirname, '..'),
    entry: {
        bundle: [
            'react-hot-loader/patch',
            'webpack-hot-middleware/client?path=http://' + host + ':' + port + '/__webpack_hmr',
            './src/client.js'
        ]
    },
    output: {
        path: assetsPath,
        publicPath: publicPath,
        filename: jsName,
        chunkFilename: jsChunkName
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader']
                })
            },
            {
                test: /\.styl$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'stylus-loader']
                })
            },
            {
                test: /\.ico$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]'
                }
            },
            {
                test: /\.(jpg|jpeg|gif|png)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: 'images/[name].[ext]'
                }
            },
            {test: /\.svg/, loader: 'url-loader?limit=26000&mimetype=image/svg+xml'},
            {test: /\.(woff|woff2|ttf|eot)/, loader: 'url-loader?limit=1'},
            {
                test: /\.jsx?$/,
                loaders: ['react-hot-loader/webpack','babel-loader'],
                exclude: [/node_modules/]
            },
        ]
    },
    resolve: {
        modules: ['src', 'node_modules'],
        extensions: ['.js', '.jsx', '.styl']
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        // Загрузка CSS
        new ExtractTextPlugin({filename: cssName, allChunks: true}),
        new webpack.DefinePlugin({
            'process.env': {
                BROWSER: true
            }
        }),
        new ManifestPlugin({
            writeToFileEmit: true,
            fileName: '../../manifest.json'
        })
    ]
};