import 'babel-polyfill';

import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import CompressionPlugin from 'compression-webpack-plugin';
import ManifestPlugin from 'webpack-manifest-plugin';

const projectRootPath = path.resolve(__dirname, '../'),
    buildPath = path.resolve(projectRootPath, './build/client'),
    jsName = '[name].[chunkhash].js',
    compressedJsName = '[path].gz',
    cssName = '[name].[chunkhash].css';

module.exports = {
    devtool: 'source-map',
    context: path.resolve(__dirname, '..'),
    entry: {
        bundle: [
            'babel-polyfill',
            './src/client.js'
        ]
    },
    output: {
        path: buildPath,
        publicPath: '/dist/',
        filename: jsName
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader']
                })
            },
            {
                test: /\.styl$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'stylus-loader']
                })
            },
            {
                test: /\.(jpg|jpeg|gif|png)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: 'images/[name].[hash].[ext]'
                }
            },
            {
                test: /\.ico$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]'
                }
            },
            {test: /\.svg/, loader: 'url-loader?limit=26000&mimetype=image/svg+xml'},
            {test: /\.(woff|woff2|ttf|eot)/, loader: 'url-loader?limit=1'},
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: [/node_modules/]
            },
        ]
    },
    resolve: {
        modules: ['src', 'node_modules'],
        extensions: ['.js', '.jsx', '.styl']
    },
    plugins: [
        // Чистка директории
        new CleanWebpackPlugin([buildPath], {root: projectRootPath}),
        // Загрузка CSS
        new ExtractTextPlugin({filename: cssName, allChunks: true}),
        // Глобальные константы
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"',
                PROD: true,
                BROWSER: true
            }
        }),
        new webpack.LoaderOptionsPlugin({
            debug: true,
            minimize: true,
            options: {
                progress: true
            }
        }),
        new webpack.optimize.UglifyJsPlugin(),
        // Игнор дев плагинов (не уверен насчет config)
        new webpack.IgnorePlugin(/\.\/dev/,/\/config$/),
        new webpack.IgnorePlugin(/\.\/locale/,/moment$/),
        // Компрессия
        new CompressionPlugin({
            asset: compressedJsName,
            algorithm: 'gzip',
            minRatio: 0.8
        }),
        new ManifestPlugin({
            fileName: 'manifest.json'
        })
    ]
};