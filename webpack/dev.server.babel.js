import 'babel-polyfill';
import Express from 'express';
import webpack from 'webpack';
import config from '../src/server/config';
import webpackConfig from './dev.client.babel';
import WebpackDevMiddleware from 'webpack-dev-middleware';
import WebpackHotMiddleware from 'webpack-hot-middleware';

const compiler = webpack(webpackConfig),
    host = config.host || 'localhost',
    port = (+config.port + 1) || 3001,
    serverOptions = {
        contentBase: 'http://' + host + ':' + port,
        noInfo: true,
        hot: true,
        inline: true,
        lazy: false,
        publicPath: webpackConfig.output.publicPath,
        headers: {'Access-Control-Allow-Origin': '*'},
        stats: {colours: true}
    },
    app = new Express();

app
    .use(WebpackDevMiddleware(compiler, serverOptions))
    .use(WebpackHotMiddleware(compiler))
    .listen(port, err =>
        err
            ? console.error(err)
            : console.info('==> Webpack development server listening on port %s', port)
    );