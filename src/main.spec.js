import mongoose from 'server/mongoose';

after(done => {
    mongoose.connection.close();
    done();
});