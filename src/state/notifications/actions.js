export const NTF_ADD = 'notifications/add';
export const NTF_REMOVE = 'notifications/remove';

export function removeNtf(id) {
    return (dispatch => {
        dispatch({
            type: NTF_REMOVE,
            data: {
                id
            }
        });
    });
}