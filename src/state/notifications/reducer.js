import {NTF_ADD, NTF_REMOVE} from './actions';
const config = process.env.BROWSER ? require('../../client/config') : require('../../server/config');

const initialState = {
    notifications: [],
    lastId: 0
};

export default function (state = initialState, action) {
    switch (action.type) {
        case NTF_REMOVE:
            return Object.assign({}, state, {
                notifications: state.notifications.filter((item) => item.id !== action.data.id)
            }
            );
        case NTF_ADD:
        default:
            if (action.ntf) {
                const ntf = action.ntf;
                return Object.assign({}, state, {
                    notifications: [
                        {
                            id: state.lastId,
                            ...ntf
                        },
                        ...state.notifications
                    ].slice(0, config.ntf.max),
                    lastId: state.lastId + 1
                }
                );
            }
            return state;
    }
}