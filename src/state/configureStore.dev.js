import {applyMiddleware, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import createSocketIoMiddleware from 'redux-socket.io';
import Socket from 'client/socket';
import {syncTranslationWithStore} from 'react-redux-i18n';

import rootReducer from './reducers';

export default function (initialState = {}) {
    const socketIoMiddleware = createSocketIoMiddleware(Socket, 'method/');

    if (typeof window !== 'undefined') {
        initialState = window.__INITIAL_STATE__ || {};
    }

    const store = createStore(rootReducer, initialState, composeWithDevTools(
        applyMiddleware(
            thunk,
            socketIoMiddleware
        )
    ));

    syncTranslationWithStore(store);

    if (module.hot) {
        module.hot.accept('./reducers', () =>
            store.replaceReducer(rootReducer.default)
        );
    }


    return store;
}