export const GROUPS_ADD_ROLE = 'method/groups/addRole';
export const GROUPS_ADD_ROLE_FAIL = 'response/groups/addRole/fail';
export const GROUPS_REMOVE_ROLE = 'method/groups/removeRole';
export const GROUPS_REMOVE_ROLE_FAIL = 'response/groups/removeRole/fail';

export function addRoleToGroup(groupID, role) {
    return (dispatch) => {
        dispatch({type: GROUPS_ADD_ROLE, data: {groupID, role}});
    };
}

export function removeRoleFromGroup(groupID, role) {
    return (dispatch) => {
        dispatch({type: GROUPS_REMOVE_ROLE, data: {groupID, role}});
    };
}