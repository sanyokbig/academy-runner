export const DRAFT_CANCEL = 'method/draft/cancel';

// Отмена драфта
export function cancelDraft(draftId) {
    return (dispatch) => dispatch({type: DRAFT_CANCEL, data: {draftId}});
}