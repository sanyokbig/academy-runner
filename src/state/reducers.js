import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';
import {i18nReducer as i18n} from 'react-redux-i18n';

import auth from './auth/reducer';
import grid from './grid/reducer';
import notifications from './notifications/reducer';
import subs from './subs/reducer';

export default combineReducers({
    form,
    i18n,
    auth,
    grid,
    notifications,
    subs
});