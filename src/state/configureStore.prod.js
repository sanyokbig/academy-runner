import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import createSocketIoMiddleware from 'redux-socket.io';
import Socket from 'client/socket';
import {syncTranslationWithStore} from 'react-redux-i18n';

import rootReducer from './reducers';

export default function (initialState = {}) {
    let socketIoMiddleware = createSocketIoMiddleware(Socket, 'method/');

    if (typeof window !== 'undefined') {
        initialState = window.__INITIAL_STATE__ || {};
    }

    const store = createStore(rootReducer, initialState,
        applyMiddleware(
            thunk,
            socketIoMiddleware
        ));

    syncTranslationWithStore(store);

    return store;
}