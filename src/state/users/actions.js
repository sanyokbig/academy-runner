export const USERS_ADD_TO_GROUP = 'method/user/addToGroup';
export const USERS_REMOVE_FROM_GROUP = 'method/user/removeFromGroup';
export const USERS_REMOVE = 'method/user/remove';

export function addToGroup(characterID, groupID) {
    return (dispatch) => {
        dispatch({
            type: USERS_ADD_TO_GROUP, data: {
                characterID,
                groupID
            }
        });
    };
}

export function removeFromGroup(characterID, groupID) {
    return (dispatch) => {
        dispatch({
            type: USERS_REMOVE_FROM_GROUP, data: {
                characterID,
                groupID
            }
        });
    };
}

export function removeUser(characterID) {
    return (dispatch) => {
        dispatch({
            type: USERS_REMOVE, data: {
                characterID
            }
        });
    };
}