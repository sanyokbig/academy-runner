import {matchRoutes} from 'react-router-config';
import {loadTranslations, setLocale} from 'react-redux-i18n';
import _ from 'lodash';

import routesData from '../routesData';
import configureStore from 'state/configureStore';
import Translator from 'server/modules/Translator';

// Генерация редакс сторы для текущего пути
export const prepareStore = async (req, res, next) => {
    // Ручноая подготовка стейта
    const initState = {};

    // Записываем аутентификацию
    initState.auth = req.authState || null;

    // Загружаем нужные данные для этой части приложения
    const branch = matchRoutes(routesData, req.url);

    // Проходим по всей ветке
    const data = await Promise.all(branch.map(({route, match}) => {
        return route.loadData
            ? route.loadData(match, req.sessionID)
            : Promise.resolve(null);
    }));

    // Объединяем полученные данные
    data.map(_d => _.merge(initState, _d));

    // Генерируем стору
    const store = configureStore(initState);

    // Настраиваем i18n
    store.dispatch(loadTranslations(Translator.getTranslations(['ru'])));
    store.dispatch(setLocale('ru'));

    // Запись сторы в запрос для использования позже
    req.preparedStore = store;

    next();
};