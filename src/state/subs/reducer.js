import update from 'immutability-helper';
import {SUBS_CURRENT, SUBS_REMOVED, SUBS_SUBSCRIBE_OK, SUBS_UPDATED, SUBS_UNSUBSCRIBE} from './actions';

const initialState = {};

export default function (state = initialState, action) {
    switch (action.type) {
        case SUBS_SUBSCRIBE_OK:
        // Запись успешной подписки в список
            return update(state, {
                [action.data.topic]: {$set: null}
            });

        case SUBS_UPDATED: {
        // Добавление/обновление документа
            const {topic, doc, uniqueField} = action.data;

            if(!state[topic]) {
                return state;
            }

            const index = state[topic].findIndex(item => item[uniqueField] === doc[uniqueField]),
                setter = index === -1
                    ? {
                        $push: [doc]
                    }
                    : {
                        [index]: {$set: doc}
                    };

            return update(state, {
                [action.data.topic]: setter
            });
        }

        case SUBS_REMOVED: {
        // Удаление документа
            let {topic, doc, uniqueField} = action.data;

            if(!state[topic]) {
                return state;
            }

            const index = state[topic].findIndex(item => item[uniqueField] === doc[uniqueField]);

            return update(state, {
                [topic]: {
                    $splice: [[index, 1]]
                }
            });
        }

        case SUBS_CURRENT:
        // Получение текущих документов
            return update(state, {
                [action.data.topic]: {
                    $set: action.data.docs
                }
            });

        case SUBS_UNSUBSCRIBE: {
            const {[action.data.topic]: remove, ...rest} = state;
            return rest;
        }
        default:
            return state;
    }
}