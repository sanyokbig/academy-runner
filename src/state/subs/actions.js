export const SUBS_SUBSCRIBE = 'method/subs/subscribe';
export const SUBS_SUBSCRIBE_OK = 'response/subs/subscribe/ok';
export const SUBS_SUBSCRIBE_FAIL = 'response/subs/subscribe/fail';
export const SUBS_UNSUBSCRIBE = 'subs/unsubscribe';

export const SUBS_UPDATED = 'subs/updated';
export const SUBS_REMOVED = 'subs/removed';
export const SUBS_CURRENT = 'subs/current';

export const subscribeAction = (topic) => {
    return (dispatch) => {
        dispatch({type: SUBS_SUBSCRIBE, data: {topic}});
    };
};