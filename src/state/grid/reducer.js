import {GRID_SORT, GRID_REGISTER, GRID_TURN_PAGE, GRID_SEARCH, GRID_CHANGE_SETTING} from './actions';

const initialState = {};

export const initialGridState = (data={}) => ({
    sortKey: null,
    sortDir: 1,
    page: 1,
    searchFields: data.searchFields || null,
    searchQuery: '',
    usePagination: true
});

export default function (state = initialState, action) {
    switch (action.type) {
        case GRID_SORT:
            return Object.assign({}, state, {
                [action.data.name]: {
                    ...state[action.data.name],
                    ...action.data.sort
                }
            });
        case GRID_TURN_PAGE:
            return Object.assign({}, state, {
                [action.data.name]: {
                    ...state[action.data.name],
                    page: action.data.selected
                }
            });
        case GRID_SEARCH:
            return Object.assign({}, state, {
                [action.data.name]: {
                    ...state[action.data.name],
                    ...action.data.search
                }
            });
        case GRID_REGISTER:
            if (!state[action.data.name]) {
                return Object.assign({}, state, {
                    [action.data.name]: initialGridState(action.data)
                });
            }
            return state;
        case GRID_CHANGE_SETTING: {
            const {name, ...settings} = action.data;
            return Object.assign({}, state, {
                [action.data.name]: {
                    ...state[action.data.name],
                    ...settings
                }
            });
        }
        default:
            return state;
    }
}