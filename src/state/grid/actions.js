export const GRID_SORT = 'grid/sort';
export const GRID_SEARCH = 'grid/search';
export const GRID_TURN_PAGE = 'grid/turnPage';
export const GRID_REGISTER = 'grid/register';
export const GRID_CHANGE_SETTING= 'grid/setting';

export function registerGrid(data) {
    return (dispatch) => {
        dispatch({type: GRID_REGISTER, data: data});
    };
}

export function sort(data) {
    return (dispatch) => {
        dispatch({type: GRID_SORT, data});
    };
}

export function search(data) {
    return (dispatch) => {
        dispatch({type: GRID_SEARCH, data});
    };
}

export function turnPage(data) {
    return (dispatch) => {
        dispatch({type: GRID_TURN_PAGE, data});
    };
}

export function changeSetting(data) {
    return (dispatch) => {
        dispatch({type: GRID_CHANGE_SETTING, data});
    };
}