import {
    AUTH_LOGOFF_OK,
    AUTH_ROLES,
    AUTH_STATE
} from './actions';
import update from 'immutability-helper';

const initialState = {
    loggedIn: false,
    user: {},
    roles: []
};

export default function (state = initialState, action) {
    switch (action.type) {
        case AUTH_LOGOFF_OK:
            return Object.assign({}, state, initialState);
        case AUTH_STATE:
            return Object.assign({}, state, action.data);
        case AUTH_ROLES:
            return update(state, {roles: {$set: action.data.roles}});
        default:
            return state;
    }
}