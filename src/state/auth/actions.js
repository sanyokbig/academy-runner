export const AUTH_LOGIN = 'method/auth/login';
export const AUTH_LOGOFF = 'method/auth/logoff';
export const AUTH_LOGOFF_OK = 'auth/logoff/ok';
export const AUTH_LOGOFF_FAIL = 'auth/logoff/fail';
export const AUTH_STATE= 'auth/state';
export const AUTH_ROLES= 'auth/roles';

// Отправка запроса на логин на сервер
export function login(scopes) {
    return (dispatch) => dispatch({type: AUTH_LOGIN, data: {scopes}});
}

export function logoffRequest() {
    return (dispatch) => dispatch({type: AUTH_LOGOFF});
}