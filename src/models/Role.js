import mongoose from 'server/mongoose';

const Schema = mongoose.Schema;

const RoleSchema = new Schema({
    _id: String, // Системное имя
    displayName: String // Отображаемое имя
});

// Роль - ее налоичие проверяетсяч при попытке доступа
const Role = mongoose.model('role', RoleSchema);

export default Role;