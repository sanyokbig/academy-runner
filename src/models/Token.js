import mongoose from 'server/mongoose';
import Logger from 'server/modules/Logger';

const Schema = mongoose.Schema,
    logger = new Logger('Token');

const tokenSchema = new Schema({
    access_token: String,
    refresh_token: String,
    expires_at: Date,
    scopes: [String],
    type: String
});

tokenSchema.statics.get = async function (query) {
    try {
        return await this.find(query);
    } catch (e) {
        logger.error(e);
        return [];
    }
};

tokenSchema.statics.getOne = async function (query) {
    try {
        return await this.findOne(query);
    } catch (e) {
        logger.error(e);
        return null;
    }
};

let Token;
try {
    Token = mongoose.model('token', tokenSchema);
} catch (e) {
    Token = mongoose.model('token');
}

export default Token;








