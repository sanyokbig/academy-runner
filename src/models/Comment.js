import mongoose from 'server/mongoose';

const Schema = mongoose.Schema;

//Комментарий о пилоте
const Comment = mongoose.model('comment', new Schema({
    type: Number, // Тип комментария - предупреждение или комментарий
    author: { // Автор
        type: String,
        ref: 'user'
    },
    user: { // Целевой пилот
        type: String,
        ref: 'user'
    },
    message: String, // Сам комментарий
    posted: Date, // Дата создания
}));

export default Comment;