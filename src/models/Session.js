import mongoose from 'server/mongoose';
import Logger from 'server/modules/Logger';
import {NOT_LOGGED_IN} from 'server/constants';

const logger = new Logger('Session');

const Schema = mongoose.Schema;

const sessionSchema = new Schema({
    _id: String,
    session: {
        state: String,
        cookie: Object,
        logged: Boolean, // Залогинен ли юзер сессии
        loggedAs: { // Залогинен как ...
            ref: 'user',
            type: Schema.Types.ObjectId
        },
        roles: [String], // Роли сессии
        sockets: [String] // Сокеты сессии
    },
    expires: Date
});

sessionSchema.statics.get = async (_id) => {
    try {
        return await Session.findOne({_id});
    } catch (e) {
        logger.error(e.message);
        return null;
    }
};

sessionSchema.statics.getSockets = async function (_id) {
    let session;
    try {
        session = await this.findOne({_id});
    } catch (e) {
        logger.error(e.message);
    }

    return session ? session.session.sockets : [];
};

sessionSchema.methods.login = async function (userId, roles) {
    this.set({
        session: {
            logged: true,
            loggedAs: userId,
            roles: roles
        }
    });

    try {
        await this.save();
    } catch (e) {
        logger.error(e);
        throw new Error('session login failed');
    }

    return true;
};

sessionSchema.methods.logoff = async function () {
    this.set({
        session: {
            logged: false,
            loggedAs: null,
            roles: []
        }
    });

    try {
        await this.save();
    } catch (e) {
        logger.error(e);
        throw new Error('session logoff failed');
    }

    return true;
};

sessionSchema.methods.getAuthState = async function () {
    try {
        await this.populate({path: 'session.loggedAs', populate: {path: 'groups'}}).execPopulate();
    } catch (e) {
        logger.error(e);
        return NOT_LOGGED_IN;
    }

    if (!this.session.loggedAs) {
        logger.result('Session', this._id, 'not authenticated');
        return NOT_LOGGED_IN;
    }

    const loggedAs = this.session.loggedAs;

    // Генерация массива ролей
    return {
        loggedIn: true,
        roles: await loggedAs.getRoles(),
        user: {
            characterID: loggedAs.characterID,
            characterName: loggedAs.characterName
        }
    };
};

sessionSchema.methods.haveAccess = function (role) {
    return this.session.logged && this.session.roles.includes(role);
};

let Session;

try {
    Session = mongoose.model('session', sessionSchema);
} catch (e) {
    Session = mongoose.model('session');
}

export default Session;