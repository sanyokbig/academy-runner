import mongoose from 'server/mongoose';
import Connector from 'server/modules/Connector';
import Logger from 'server/modules/Logger';
import './User';

const Schema = mongoose.Schema,
    logger = new Logger('Income');

const incomeSchema = new Schema({
    _id: String,
    characterID: Number,
    date: Date,
    amount: Number
}, {
    toJSON: {virtuals: true},
    toObject : {virtuals:true}
});

incomeSchema.virtual('character', {
    ref: 'user',
    localField: 'characterID',
    foreignField: 'characterID',
    justOne: true
});

incomeSchema.statics.get = async function (query) {
    try {
        return await this.find(query);
    } catch (e) {
        logger.error(e);
        return [];
    }
};

incomeSchema.statics.getOne = async function (query) {
    try {
        return await this.findOne(query);
    } catch (e) {
        logger.error(e);
        return null;
    }
};

Connector.connect('income', incomeSchema);

let Income;
try {
    Income = mongoose.model('income', incomeSchema);
} catch (e) {
    Income = mongoose.model('income');
}

export default Income;