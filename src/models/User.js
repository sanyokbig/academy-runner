import mongoose from 'server/mongoose';

import Connector from 'server/modules/Connector';
import Logger from 'server/modules/Logger';
import config from 'server/config';

const Schema = mongoose.Schema,
    logger = new Logger('User');

const userSchema = new Schema({
    groups: [{
        ref: 'group',
        type: String,
        set: function (groups) {
            this._authRulesChanged = true;
            this._groups = this.groups;
            return groups;
        }
    }],
    _groups: [{
        ref: 'group',
        type: String
    }],
    token: {
        ref: 'token',
        type: Schema.ObjectId
    },
    // Информация по персонажу юзера
    characterID: Number,
    characterName: String,
    // Текущая локация
    locationID: Number,
    location: String,
    // Текущий шип
    shipTypeID: Number,
    shipType: String,
    // Даты
    startDateTime: Date,
    logonDateTime: Date,
    logoffDateTime: Date,
    // Суммырнае статы за время в академии
    losses: Number,
    kills: Number,
    efficiency: Number, // КПД по киллам и лоссам kills / (kills + losses)
    points: Number, // Очки ZKB
    income: Number, // Доход
    // Информация о драфте
    _draft: Object
});

Connector.connect('user', userSchema);

userSchema.statics.get = async function (query) {
    try {
        return await this.find(query);
    } catch (e) {
        logger.error(e);
        return [];
    }
};

userSchema.statics.getOne = async function (query) {
    try {
        return await this.findOne(query);
    } catch (e) {
        logger.error(e);
        return null;
    }
};

userSchema.statics.resetPrevQuery = async function (_id) {
    await this.update({_id}, {$set: {_groups: null}});
};

userSchema.statics.prepareForLogin = async function (characterID, characterName = 'empty characterName') {
    if (!characterID) {
        logger.error('User creation aborted: characterID missing');
        throw new Error('missing characterID');
    }

    let user = await this.getOne({'characterID': +characterID});

    // Если не существует, создаем нового
    if (!user) {
        logger.info('Creating user with data:', JSON.stringify({characterID, characterName}));
        user = new this({
            characterID: characterID,
            characterName: characterName,
            groups: config.access.predefinedAdmins.includes(characterID) ? ['admin'] : ['user']
        });

        logger.result('Successfully created user: ', JSON.stringify(user));
    }

    return user;
};

userSchema.methods.getToken = async function () {
    if (this.token) {
        return await this.populate('token').execPopulate() && this.token;
    }

    return null;
};

userSchema.methods.getRoles = async function () {
    try {
        await this.populate({path: 'groups'}).execPopulate();
    } catch (e) {
        logger.error(e);
        return [];
    }

    return []
        .concat(...this.groups.map(group => group.roles)) // Список ролей
        .filter((val, index, array) => array.indexOf(val) === index); // Удаление повторений
};

userSchema.methods.addToGroup = async function (group) {
    logger.info('Adding user', this.characterName, 'to group', group);

    // Юзер уже в группе
    if (this.groups.includes(group)) {
        logger.alert('User already in group');
        throw new Error('user already in group');
    }

    // Если группы нет, добавляем группу и сохраняем
    this.groups.push(group);

    try {
        await this.save();
    } catch (e) {
        logger.error(e);
        throw new Error('mongo error');
    }

    //Сохранение успешно
    logger.result('User', this.characterName, 'added to', group, 'group');
};

userSchema.methods.removeFromGroup = async function (group) {
    logger.info('Removing user', this.characterName, 'from group', group);

    // Юзер не в группе
    if (!this.groups.includes(group)) {
        logger.alert('User is not in group');
        throw new Error('user is not in group');
    }

    // Если группа есть, удаляем группу и сохраняем
    this.groups = this.groups.filter(item => item !== group);

    try {
        await this.save();
    } catch (e) {
        logger.error(e);
        throw new Error('mongo error');
    }

    //Сохранение успешно
    logger.result('User', this.characterName, 'removed from', group, 'group');
};

userSchema.methods.removeUser = async function () {
    logger.info('Removing user', this.characterName);

    try {
        await this.remove();
    } catch (e) {
        logger.error(e);
        throw new Error('mongo error');
    }

    //Сохранение успешно
    logger.result('User', this.characterName, 'removed');
};

let User;
try {
    User = mongoose.model('user', userSchema);
} catch (e) {
    User = mongoose.model('user');
}

export default User;