import mongoose from 'server/mongoose';
import Connector from 'server/modules/Connector';
import Logger from 'server/modules/Logger';

const Schema = mongoose.Schema,
    logger = new Logger('Kill');

const killSchema = new Schema({
    killID: Number, // Номер килла
    type: String, // Тип килла, 'kill' / 'loss'
    date: Date, // Дата
    totalValue: Number, // ЗКБ стоимость килла
    points: Number, // ЗКБ вес килла
    npc: Boolean, // ЗКБ ПВЕ слив
    involved: [{
        type: Number,
        ref: 'user'
    }]
});


killSchema.statics.get = async function (query) {
    try {
        return await this.find(query);
    } catch (e) {
        logger.error(e);
        return [];
    }
};

killSchema.statics.getOne = async function (query) {
    try {
        return await this.findOne(query);
    } catch (e) {
        logger.error(e);
        return null;
    }
};

Connector.connect('kill', killSchema);

let Kill;
try {
    Kill = mongoose.model('kill', killSchema);
} catch(e) {
    Kill = mongoose.model('kill');
}

export default Kill;