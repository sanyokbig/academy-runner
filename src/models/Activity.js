import mongoose from 'server/mongoose';

const Schema = mongoose.Schema;

//Академ. мероприятие - КТА, ОПС, роум, лекция
const Activity = mongoose.model('activity', new Schema({
    type: String, // Тип мероприятия
    title: String, // Название от создающего
    fleetcom: { // Создающий запись, если не КТА, то он же и ФК
        type: String,
        ref: 'user'
    },
    posted: Date, // Дата создания записи
    conducted: Date, // Дата поведения
}));

export default Activity;