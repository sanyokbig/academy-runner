import mongoose from 'server/mongoose';

import Connector from 'server/modules/Connector';
import Logger from 'server/modules/Logger';

const Schema = mongoose.Schema,
    logger = new Logger('Group');

const groupSchema = new Schema({
    _id: String,
    name: String, // Отображаемое имя группы
    roles: {
        type: [String],
        set: function (roles) {
            this._authRulesChanged = true;

            return roles;
        }
    }
});

groupSchema.statics.get = async function (query) {
    try {
        return await this.find(query);
    } catch (e) {
        logger.error(e);
        return [];
    }
};

groupSchema.statics.getOne = async function (query) {
    try {
        return await this.findOne(query);
    } catch (e) {
        logger.error(e);
        return null;
    }
};

groupSchema.methods.addRole = async function (role) {
    logger.info('Adding role', role, 'to group', this._id);

    // Если роль имеется, кидаем ошибку
    if (this.roles.includes(role)) {
        logger.alert('group' + this._id + 'already have role' + role);
        throw new Error('group already have role');
    }

    // Роли нет, добавляет
    this.roles.push(role);

    // Пробуем сохранить
    try {
        await this.save();
    } catch (e) {
        logger.error(e);
        throw new Error('group save failed');
    }

    logger.result('Role', role, 'added to', this._id, 'group');
    return this;
};

groupSchema.methods.removeRole = async function (role) {
    logger.info('Removing role', role, 'from group', this._id);

    // Если роли нет, кидаем ошибку
    if (!this.roles.includes(role)) {
        logger.alert('group' + this._id + 'have no role' + role);
        throw new Error('group have no role');
    }

    // Роль есть, выпиливыаем
    this.roles = this.roles.filter(_role => _role!== role);

    // Пробуем сохранить
    try {
        await this.save();
    } catch (e) {
        logger.error(e);
        throw new Error('group save failed');
    }

    logger.result('Role', role, 'removed from', this._id, 'group');
    return this;
};


Connector.connect('group', groupSchema);

let Group;
try {
    Group = mongoose.model('group', groupSchema);
} catch (e) {
    Group = mongoose.model('group');
}

export default Group;