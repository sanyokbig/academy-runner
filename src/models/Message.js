import mongoose from 'mongoose';

const Schema = mongoose.Schema;


export const Message = mongoose.model('message', new Schema({
    message: String,
    date: Date
}));
