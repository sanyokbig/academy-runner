import React from 'react';
import Layout from './layouts/Layout';
import Routes from '../routes';

export default () => (
    <Layout>
        <Routes/>
    </Layout>
);