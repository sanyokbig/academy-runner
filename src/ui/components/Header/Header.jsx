import React from 'react';
import {Link} from 'react-router-dom';
import {Navbar, NavItem} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import {Translate as T} from 'react-redux-i18n';

import PrivateNav from 'ui/components/PrivateNav';
import {AuthDropdown} from 'ui/components/Auth';

const Header = () => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <Link to='/'><T value="application.short"/></Link>
            </Navbar.Brand>
            <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>

            <PrivateNav require="pilotsRead" navbar>
                <LinkContainer to="/pilots">
                    <NavItem><T value='pilots.plural'/></NavItem>
                </LinkContainer>
            </PrivateNav>

            <PrivateNav require="incomesRead" navbar>
                <LinkContainer to="/incomes">
                    <NavItem><T value='incomes.plural'/></NavItem>
                </LinkContainer>
            </PrivateNav>

            <PrivateNav require="usersRead" navbar>
                <LinkContainer to="/users">
                    <NavItem><T value='users.plural'/></NavItem>
                </LinkContainer>
            </PrivateNav>

            <PrivateNav require="groupsRead" navbar>
                <LinkContainer to="/groups">
                    <NavItem><T value='groups.plural'/></NavItem>
                </LinkContainer>
            </PrivateNav>

            <AuthDropdown/>
        </Navbar.Collapse>
    </Navbar>
);

export default Header;