import React from 'react';
import {Image} from 'react-bootstrap';

import AssetManager from 'universal/AssetManager';

import './style.styl';

const CharacterName = ({characterID, characterName}) =>
    <div className="character-name">
        <a
            key="img"
            className='character-name-img-wrapper'
            href={AssetManager.zkb(characterID)}
            target="_blank"
        >
            <Image
                height="32"
                src={AssetManager.portrait(characterID)}
            />
        </a>
        <a
            key="name"
            href={AssetManager.zkb(characterID)}
            target="_blank"
        >
            {characterName}
        </a>

    </div>;

export default CharacterName;