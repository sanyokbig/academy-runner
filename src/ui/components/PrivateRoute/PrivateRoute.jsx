import React from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

// Приватный рут - принимает доп. параметр require, роль, необходимая для доступа к руту
// Если доступ не получен - редирект на главную
const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        rest.roles.includes(rest.require) ? (
            <Component {...props}/>
        ) : (
            <Redirect to={{
                pathname: '/',
                state: { from: props.location }
            }}/>
        )
    )}/>
);

const mapStateToProps = (state) => {
    const {roles} = state.auth;

    return {roles};
};

export default connect(mapStateToProps)(PrivateRoute);