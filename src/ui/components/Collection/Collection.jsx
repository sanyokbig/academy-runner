import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button} from 'react-bootstrap';
import Menu, {Item} from 'rc-menu';
import Dropdown from 'rc-dropdown';

import 'rc-dropdown/assets/index.css';
import 'font-awesome/css/font-awesome.css';
import './style.styl';

const propTypes = {
    current: PropTypes.array.isRequired,
    total: PropTypes.array.isRequired,
    addAction: PropTypes.func.isRequired,
    removeAction: PropTypes.func.isRequired,
    sorter: PropTypes.func,
    key: PropTypes.string,
    displayKey: PropTypes.string,
    renderer: PropTypes.func,
    data: PropTypes.object.isRequired,
    idKey: PropTypes.string
};

class Collection extends Component {
    constructor() {
        super();
    }

    defaultSorter = (a, b) => (a > b);

    render() {
        const {current, total, addAction, removeAction, sorter, idKey, displayKey, data, renderer} = this.props;
        //Сортировкак списка
        current.sort(sorter || this.defaultSorter);

        //Текущий список с возможностью удаления элементов
        const currentElements = [];
        const collection = current.map(element => {
            let id = idKey ? element[idKey] : element;
            currentElements.push(id);
            return (
                <Button
                    className="collection-element"
                    key={id}
                    bsSize="xs"
                    title={id}
                >
                    {renderer
                        ? (renderer(element))
                        : (displayKey ? element[displayKey] : element)
                    }
                    <i className="fa fa-times collection-remove-element"
                        onClick={() => removeAction(data, id)}/>
                </Button>
            );
        });

        //Список элементов, которые можно добавить
        const elementsToAdd = total
            .filter(element => !currentElements.includes(idKey ? element[idKey] : element))
            .map((element) =>
                <Item
                    key={idKey ? element[idKey] : element}
                >
                    {renderer
                        ? (renderer(element))
                        : (displayKey ? element[displayKey] : element)
                    }
                </Item>
            );

        const menu = (
            <Menu
                onClick={({key})=>addAction(data, key)}
            >
                {elementsToAdd}
            </Menu>);

        // Кнопка добавления элементов
        elementsToAdd.length && collection.push(
            <Dropdown
                trigger={['click']}
                key="add"
                animation="slide-up"
                overlay={menu}
            >
                <Button bsSize="xs" className="collection-element">
                    <i className="fa fa-plus collection-add-element"/>
                </Button>
            </Dropdown>
        );

        return (
            <div className="collection">
                {collection}
            </div>
        );
    }
}

Collection.propTypes = propTypes;

export default Collection;