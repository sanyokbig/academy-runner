import React from 'react';
import {connect} from 'react-redux';
import {Nav} from 'react-bootstrap';

// Приватный нав - принимает доп. параметр require, роль, необходимая для доступа к наву
// Если доступ не получен - не показывать
const PrivateNav = ({children, require, roles, dispatch, ...rest}) => (
    roles.includes(require) && (
        <Nav {...rest}>
            {children}
        </Nav>
    )
);

const mapStateToProps = (state) => {
    const {roles} = state.auth;

    return {roles};
};

export default connect(mapStateToProps)(PrivateNav);