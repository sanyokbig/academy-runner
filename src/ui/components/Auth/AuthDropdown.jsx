import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Translate as T} from 'react-redux-i18n';
import {Nav, NavDropdown, Image, MenuItem} from 'react-bootstrap';

import AssetManager from 'universal/AssetManager';
import eveAuthClient from 'client/eveAuth';

import {logoffRequest, login as actionLogin} from 'state/auth/actions';

import 'static/images/empty.jpg';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    loggedIn: PropTypes.bool.isRequired,
    user: PropTypes.object.isRequired,
    roles: PropTypes.array
};

class AuthDropdown extends Component {
    constructor() {
        super();
    }

    logoff = () => {
        this.props.dispatch(logoffRequest());
    };

    loginAsReader = () => {
        eveAuthClient.prepareLogin();
        this.props.dispatch(actionLogin(['corporationMembersRead', 'corporationWalletRead']));
    };

    login = () => {
        eveAuthClient.prepareLogin();
        this.props.dispatch(actionLogin(['publicData']));
    };

    characterPortrait = () => {
        if (this.props.loggedIn) {
            return AssetManager.portrait(this.props.user.characterID);
        } else {
            return AssetManager.image('empty.jpg');
        }
    };

    render() {
        return (
            <Nav pullRight>
                <NavDropdown className="authDropdown" id="1"
                    title={<Image height="32" src={this.characterPortrait()}/>}>
                    {this.props.loggedIn && this.props.user.characterName && (
                        <MenuItem header>{this.props.user.characterName}</MenuItem>
                    )}

                    {this.props.loggedIn && this.props.roles.includes('eveApiReader') && (
                        <MenuItem onClick={this.loginAsReader}><T value="auth.loginAsReader"/></MenuItem>
                    )}

                    {this.props.loggedIn ? (
                        <MenuItem onClick={this.logoff}><T value="auth.logoff"/></MenuItem>
                    ) : (
                        <MenuItem onClick={this.login}><T value="auth.login"/></MenuItem>
                    )}

                </NavDropdown>
            </Nav>
        );
    }
}

AuthDropdown.propTypes = propTypes;

function mapStateToProps(state) {
    const {loggedIn, user, roles} = state.auth;

    return {loggedIn, user, roles};
}

export default connect(mapStateToProps)(AuthDropdown);