import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Translate as T} from 'react-redux-i18n';
import {Button} from 'react-bootstrap';

import {logoffRequest} from 'state/auth/actions';

const propTypes = {
    dispatch: PropTypes.func.isRequired
};

class LogoffButton extends Component {
    constructor() {
        super();

        this.logoff = this.logoff.bind(this);
    }

    logoff() {
        this.props.dispatch(logoffRequest());
    }

    render() {
        return (
            <div>
                <Button onClick={this.logoff}><T value="auth.logoff"/></Button>
            </div>
        );
    }
}

LogoffButton.propTypes = propTypes;

function mapStateToProps() {
    return {};
}

export default connect(mapStateToProps)(LogoffButton);