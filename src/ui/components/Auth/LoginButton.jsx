import React from 'react';
import {Button} from 'react-bootstrap';
import {Translate as T} from 'react-redux-i18n';

const login = function () {
    let width = 800,
        height = 600,
        x = screen.width / 2 - width / 2,
        y = screen.height / 2 - height / 2;
    window.open('http://localhost:3001/auth', 'eveAuth', 'menubar=0,width=' + width + ',height=' + height + ',left=' + x + ',top=' + y);
};

const LoginButton = () => (
    <div>
        <Button onClick={login}><T value="auth.login"/></Button>
    </div>
);

export default LoginButton;