import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

import Single from './Single';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    notifications: PropTypes.array.isRequired
};

class Notifications extends Component {
    constructor() {
        super();
    }

    render() {
        const list = this.props.notifications.map(ntf => {
            return (
                <Single
                    data={ntf}
                    key={ntf.id}
                />
            );
        });

        return (
            <div className="notifications-container">
                <CSSTransitionGroup transitionName="notification" transitionEnterTimeout={500}
                    transitionLeaveTimeout={300}>
                    {list}
                </CSSTransitionGroup>
            </div>
        );
    }
}

Notifications.propTypes = propTypes;

function mapStateToProps(state) {
    const {notifications} = state.notifications;

    return {notifications};
}

export default connect(mapStateToProps)(Notifications);