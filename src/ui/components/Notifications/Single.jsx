import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Translate as T} from 'react-redux-i18n';

import {removeNtf} from 'state/notifications/actions';

// noinspection NpmUsedModulesInstalled
const config = process.env.BROWSER ? require('client/config') : require('server/config');

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired
};

class Single extends Component {
    constructor() {
        super();

        setTimeout(() => {
            this.props.dispatch(removeNtf(this.props.data.id));
        }, config.ntf.lifetime);
    }

    handleClick = () => {
        this.props.dispatch(removeNtf(this.props.data.id));
    };

    render() {
        const {data} = this.props;

        return (
            <div
                className={('notification ' + (data.type || 'info'))}
                onClick={this.handleClick}
            >
                <T {...data}/>
            </div>
        );
    }
}

Single.propTypes = propTypes;

function mapStateToProps(state) {
    return state;
}

export default connect(mapStateToProps)(Single);