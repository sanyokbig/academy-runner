import React from 'react';
import PropTypes from 'prop-types';
import Dropdown from 'rc-dropdown';
import Menu, {Item} from 'rc-menu';
import {Button} from 'react-bootstrap'

export default class Actions extends React.Component {
    static props = {
        column: PropTypes.object.required,
        data: PropTypes.object.required
    };

    constructor(props) {
        super();

        const actions = {};
        props.column.actions.map(action => actions[action.key] = action.action);

        this.state = {
            actionsVisible: false,
            actions
        };
    }

    onActionsVisibleChange = (visible) => {
        this.setState({
            actionsVisible: visible
        });
    };

    onActionClick = (data, {key}) => {
        this.setState({actionsVisible: false});
        this.state.actions[key](data);
    };

    render() {
        const {column, data} = this.props;

        // Генерация действий
        const items = column.actions.map(action => <Item key={action.key}>{action.text}</Item>);

        // Меню
        const overlay = <Menu
            selectable={false}
            onClick={this.onActionClick.bind(this, data)}
        >
            {items}
        </Menu>;

        let buttonCls = 'grid-cell-actions-toggle';

        this.state.actionsVisible && (buttonCls += '-active');

        return <Dropdown
            trigger={['click']}
            key="actions"
            animation="slide-up"
            overlay={overlay}
            visible={this.state.actionsVisible}
            onVisibleChange={this.onActionsVisibleChange}
        >
            <div
                className={buttonCls}
            >
                <i
                    className='fa fa-ellipsis-h'
                />
            </div>
        </Dropdown>;
    }
}
