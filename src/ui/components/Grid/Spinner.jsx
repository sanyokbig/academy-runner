import React from 'react';

const Spinner = () => (
    <div className="grid-spinner">
        <i key="spinner" className="grid-spinner fa fa-spinner fa-pulse fa-3x fa-fw"/>
    </div>
);

export default Spinner;