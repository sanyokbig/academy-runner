import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Pagination as BSPagination} from 'react-bootstrap';

import {turnPage} from 'state/grid/actions';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    total: PropTypes.number.isRequired,
    current: PropTypes.number.isRequired,
    gridName: PropTypes.string.isRequired
};

class Pagination extends Component {
    constructor() {
        super();
    }

    onSelect = (selected) => {
        this.props.dispatch(turnPage({
            name: this.props.gridName,
            selected
        }));
    };

    render() {
        let shortNav = this.props.total > 2;
        return (
            <BSPagination
                items={this.props.total}
                onSelect={this.onSelect}
                activePage={this.props.current}
                next={shortNav}
                prev={shortNav}
                boundaryLinks
                maxButtons={10}
            />
        );
    }
}

Pagination.propTypes = propTypes;

export default connect()(Pagination);