import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import Undo from './Undo';
import Actions from './Actions';

import 'font-awesome/css/font-awesome.css';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    columns: PropTypes.array.isRequired
};

class Row extends Component {
    render() {
        const {data, columns} = this.props;

        //Генерируем ячейки строки
        const cells = data._draft
            ? (<Undo
                draftId={data._draft.id}
            />)
            : columns.map(column => {
                const key = column.key,
                    val = data[column.key],
                    props = {};

                let cellContent;

                if (!column.actions) {
                    props.key = key;
                    props.className = 'grid-cell ' + (column.style || 'm');
                    props.className.split(' ').includes('compact') && (props.title = val);

                    cellContent = column.renderer // Используем рендерер, если передан
                        ? (column.renderer({val, key, data}))
                        : (val);
                } else {
                    // Ячейка действий, рисуем меню с переданными действиями
                    props.key = 'actions';
                    props.className = 'grid-cell grid-cell-actions';

                    cellContent = <Actions
                        data={data}
                        column={column}
                    />;
                }
                return (
                    <div
                        {...props}
                    >
                        {cellContent}
                    </div>
                );
            });
        return (
            <div className="grid-row">
                {cells}
            </div>
        );
    }
}

Row.propTypes = propTypes;

export default connect()(Row);