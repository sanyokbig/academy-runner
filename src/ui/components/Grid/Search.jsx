import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {FormControl, InputGroup} from 'react-bootstrap';
import Dropdown from 'rc-dropdown';
import Menu, {Item} from 'rc-menu';
import {I18n} from 'react-redux-i18n';

import {search, changeSetting} from 'state/grid/actions';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    fields: PropTypes.array.isRequired,
    query: PropTypes.string.isRequired,
    gridName: PropTypes.string.isRequired,
    columns: PropTypes.array.isRequired,
    usePagination: PropTypes.bool.isRequired
};

class Search extends Component {
    constructor() {
        super();

        this.state = {
            filtersVisible: false,
            settingsVisible: false
        };
    }

    // Ввод в поле поиска
    onChange = e => {
        this.props.dispatch(search({
            name: this.props.gridName,
            search: {
                searchQuery: e.target.value
            }
        }));
    };

    // Обработчики фильтров
    onFiltersSelect = ({selectedKeys}) => {
        this.props.dispatch(search({
            name: this.props.gridName,
            search: {
                searchFields: selectedKeys
            }
        }));
    };

    onFiltersVisibleChange = (visible) => {
        this.setState({
            filtersVisible: visible
        });
    };

    // Обработчики настроек
    onSettingsChange = ({key, selectedKeys}) => {
        this.props.dispatch(changeSetting({
            name: this.props.gridName,
            [key]: selectedKeys.includes(key)
        }));
    };

    onSettingsVisibleChange = (visible) => {
        this.setState({
            settingsVisible: visible
        });
    };

    render() {
        const {query, fields, columns, usePagination} = this.props,
            items = columns.filter(_c => !_c.actions).map(column => <Item key={column.key}>{column.title}</Item>);

        // Плейсхолдер выводит список выбранных полей, либо сообщение об их отсутствии
        const placeholder = (fields.length
            ? fields.map(_f => I18n.t(columns.find(_c => _c.key === _f).title.props.value)).join(', ')
            : I18n.t('grid.selectedFieldsForSearch'))
            + '...';

        const selectedSettings = [usePagination ? 'usePagination' : ''];

        const menu = (
            <Menu
                style={{width: 120}}
                onSelect={this.onFiltersSelect}
                onDeselect={this.onFiltersSelect}
                selectedKeys={fields}
                multiple
            >
                {items}
            </Menu>
        );

        const settings = (
            <Menu
                style={{width: 120}}
                onSelect={this.onSettingsChange}
                onDeselect={this.onSettingsChange}
                selectedKeys={selectedSettings}
                multiple
            >
                <Item key="usePagination">Постранично</Item>
            </Menu>
        );

        return (
            <InputGroup>
                <FormControl
                    type="text"
                    value={query}
                    placeholder={placeholder}
                    onChange={this.onChange}
                />

                <Dropdown
                    trigger={['click']}
                    key="changeFilterField"
                    animation="slide-up"
                    overlay={menu}
                    visible={this.state.filtersVisible}
                    onVisibleChange={this.onFiltersVisibleChange}
                >
                    <InputGroup.Addon>
                        <i
                            className={'fa fa-filter' + (!fields.length ? ' no-fields-selected' : '')}
                        />
                    </InputGroup.Addon>
                </Dropdown>

                <Dropdown
                    trigger={['click']}
                    key="gridSettings"
                    animation="slide-up"
                    overlay={settings}
                    visible={this.state.settingsVisible}
                    onVisibleChange={this.onSettingsVisibleChange}
                >
                    <InputGroup.Addon>
                        <i
                            className='fa fa-gear'
                        />
                    </InputGroup.Addon>
                </Dropdown>

            </InputGroup>
        );
    }
}

Search.propTypes = propTypes;

export default connect()(Search);