import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Button} from 'react-bootstrap';

import {cancelDraft} from 'state/draft/actions';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    draftId: PropTypes.number.isRequired
};

class Undo extends Component {
    constructor() {
        super();
    }

    undo = () => {
        this.props.dispatch(cancelDraft(this.props.draftId));
    };

    render() {
        return (
            <div className='grid-cell-undo'>
                <Button
                    bsSize='xs'
                    onClick={this.undo}
                >Отменить</Button>
            </div>
        );
    }
}

Undo.propTypes = propTypes;

export default connect()(Undo);