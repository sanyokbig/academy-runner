import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import Row from './Row';
import Spinner from './Spinner';
import Pagination from './Pagination';
import Search from './Search';

import {registerGrid, sort} from 'state/grid/actions';

import './style.styl';

const config = process.env.BROWSER ? require('../../../client/config') : require('../../../server/config');

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    list: PropTypes.array,
    uniqueField: PropTypes.string,
    columns: PropTypes.array.isRequired,
    renderer: PropTypes.func,
    sorter: PropTypes.func,
    grids: PropTypes.object.isRequired,
    pageSize: PropTypes.number,
    searchFields: PropTypes.array
};

class Grid extends Component {
    // noinspection JSUnusedGlobalSymbols
    static defaultProps = {
        uniqueField: '_id',
        list: null
    };

    constructor() {
        super();
    }

    componentWillMount() {
        Object.keys(this.props.grids).includes(this.props.name) || this.props.dispatch(registerGrid(this.props));
    }

    // Обновляет состояние сортировки гриды
    sortGrid = ({key, invertDefaultSort}) => {
        const name = this.props.name;
        const grid = this.props.grids[name];

        let dir = (grid.sortKey === key) ? -grid.sortDir : (invertDefaultSort ? -1 : 1);

        this.props.dispatch(sort({
            name,
            sort: {
                sortKey: key,
                sortDir: dir
            }
        }));
    };

    // Сортировка списка перед выводом
    listSorter = (list, grid, fnc) => {
        list.sort((a, b) => {
            a = a[grid.sortKey];
            b = b[grid.sortKey];

            if (typeof a === 'undefined' || a === '') return 1;
            if (typeof b === 'undefined' || b === '') return -1;

            return fnc(a, b) * grid.sortDir;
        });
    };


    // Сравнение по умолчаниюж
    defaultSorter = (a, b) => {
        if (a > b) {
            return 1;
        } else if (a < b) {
            return -1;
        }
        return 0;
    };

    // Поиск
    search = (list, grid) => list.filter(item => grid.searchFields // Сбор всех значение элемента по выбранных полям
        .map(_f =>
            Array.isArray(item[_f]) // Если массив, то джойним
                ? item[_f].join(' ')
                : item[_f])
        .join(' ')
        .toLowerCase()
        .includes(grid.searchQuery.toLowerCase())
    );

    render() {
        const {list, columns, grids, name, uniqueField} = this.props,
            grid = grids[name] || {};
        let processedList = [...(list || [])];

        // Сортируем, если есть ключ сортировки
        grid.sortKey && this.listSorter(
            processedList,
            grid,
            columns.find(col => col.key === grid.sortKey).sorter || this.defaultSorter
        );

        // Фильтр по поисковому запросу
        grid.searchFields && (processedList = this.search(processedList, grid));

        // Поле поиска
        const search = grid.searchFields && (
            <div className="grid-search">
                <Search
                    gridName={this.props.name}
                    fields={grid.searchFields}
                    query={grid.searchQuery || ''}
                    columns={columns}
                    usePagination={grid.usePagination}
                />
            </div>
        );

        // Генерируем шапку
        const header = columns.map(column => {
            const props = {};
            let headerText, headerSortDir;

            if (!column.actions) {
                // Колонка обычная
                props.className = column.style
                    ? 'grid-cell ' + column.style
                    : 'grid-cell m';

                props.key = column.key;
                props.onClick = () => {
                    this.sortGrid(column);
                };

                headerText = column.title;
                headerSortDir = column.key === grid.sortKey
                        && (grid.sortDir === 1
                            ? <i className="fa fa-caret-up"/>
                            : <i className="fa fa-caret-down"/>)
                ;
            } else {
                // Колонка действий
                props.className = 'grid-cell grid-cell-actions';
                props.key = 'actions';
            }


            return <div
                {...props}
            >
                {headerText}
                {headerSortDir}
            </div>;
        });

        const pageSize = grid.usePagination ? this.props.pageSize || config.grid.defaultGridPageSize : (list || []).length,
            totalPages = Math.ceil(processedList.length / pageSize) || 1,
            currentPage = Math.min(grid.page ? grid.page : 1, totalPages);

        const content = [];
        if (list) {
            // Рисуем строки
            for (let i = 0, listIndex = i + (currentPage - 1) * pageSize; i < pageSize; i++, listIndex = i + (currentPage - 1) * pageSize) {
                const object = processedList[listIndex];
                content.push(listIndex < processedList.length
                    ? (
                        <Row
                            data={object}
                            columns={columns}
                            key={object[uniqueField]}
                        />)
                    : (
                        <div
                            key={i}
                        />
                    ));
            }
        } else {
            // Рисуем спиннер
            content.push(<Spinner key="0"/>);
        }

        // Пагинация
        const pagination = totalPages > 1 ? (
            <Pagination
                current={Math.min(currentPage, totalPages)}
                total={totalPages}
                gridName={this.props.name}
            />
        ) : null;

        return (
            <div className="grid">
                {search}
                <div className="scrollable">
                    <div className="grid-sorter">
                        {header}
                    </div>
                    {content}
                </div>
                {pagination}
            </div>
        );
    }
}

function mapStateToProps(state) {
    const grids = state.grid;

    return {grids};
}

Grid.propTypes = propTypes;

export default connect(mapStateToProps)(Grid);