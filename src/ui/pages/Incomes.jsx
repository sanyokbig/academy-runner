import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Translate as T} from 'react-redux-i18n';

import {Grid} from 'ui/components/Grid';
import {subscribe} from 'client/subscriber';

class Incomes extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        incomes: PropTypes.array
    };

    // noinspection JSUnusedGlobalSymbols
    static defaultProps = {
        incomes: null
    };

    constructor() {
        super();
    }

    componentWillMount() {
        subscribe('incomes');
    }

    amountRenderer = ({data}) => {
        return data.iskified;
    };

    columns = [{
        title: <T value='pilots.single'/>,
        key: 'characterName'
    }, {
        title: <T value='incomes.single'/>,
        key: 'amount',
        renderer: this.amountRenderer
    }, {
        title: <T value='common.date'/>,
        key: 'readableDate'
    }];

    render() {
        return (
            <div>
                <Grid
                    name='incomes'
                    list={this.props.incomes}
                    columns={this.columns}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    incomes: state.subs.incomes
});


export default connect(mapStateToProps)(Incomes);