import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Translate as T} from 'react-redux-i18n';

import {Grid} from 'ui/components/Grid';
import Collection from 'ui/components/Collection';
import Roles from 'server/roles';
import {subscribe} from 'client/subscriber';

import {addRoleToGroup, removeRoleFromGroup} from 'state/groups/actions';

class Groups extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        groups: PropTypes.array
    };

    // noinspection JSUnusedGlobalSymbols
    static defaultProps = {
        groups: null
    };

    constructor() {
        super();
    }

    componentWillMount() {
        subscribe('groups');
    }

    addRole = (group, role) => {
        this.props.dispatch(addRoleToGroup(group._id, role));
    };

    removeRole = (group, role) => {
        this.props.dispatch(removeRoleFromGroup(group._id, role));
    };

    rolesRenderer = ({val, data}) => (
        <Collection
            current={val}
            total={Roles}
            addAction={this.addRole}
            removeAction={this.removeRole}
            renderer={(key) => (<T value={'roles.' + key}/>)}
            data={data}
        />
    );

    columns = [{
        title: <T value='groups.single'/>,
        key: 'name'
    }, {
        title: <T value='roles.plural'/>,
        key: 'roles',
        style: 'xl',
        renderer: this.rolesRenderer
    }];

    render() {
        return (
            <div>
                <Grid
                    name='groups'
                    list={this.props.groups}
                    columns={this.columns}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        groups: state.subs.groups
    };
}

export default connect(mapStateToProps)(Groups);