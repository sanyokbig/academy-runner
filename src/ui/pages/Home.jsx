import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

const propTypes = {
    loggedIn: PropTypes.bool
};

class MainPage extends Component {
    render() {
        return (
            <div>
                {this.props.loggedIn ? (
                    <div>Лучший!</div>
                ) : (
                    <div>Так себе</div>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {loggedIn} = state.auth;

    return {loggedIn};
}

MainPage.propTypes = propTypes;

export default connect(mapStateToProps)(MainPage);