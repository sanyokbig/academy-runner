import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Translate as T} from 'react-redux-i18n';

import {Grid} from 'ui/components/Grid';
import CharacterName from 'ui/components/CharacterName';
import {subscribe} from 'client/subscriber';

import 'ui/styles/style.styl';

class Pilots extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        pilots: PropTypes.array
    };

    // noinspection JSUnusedGlobalSymbols
    static defaultProps = {
        pilots: null
    };

    constructor() {
        super();
    }

    componentWillMount() {
        subscribe('pilots');
    }

    characterNameRenderer = ({data}) => <CharacterName
        characterName={data.characterName}
        characterID={data.characterID}
    />;

    startDateTimeRenderer = ({data}) => {
        return data.timeInCorp;
    };

    logonDateTimeRenderer = ({data}) => {
        return data.sinceLastLogin;
    };

    incomeRenderer = ({data}) => {
        return data.simplifiedIncome;
    };

    columns = [{
        title: <T value='pilots.single'/>,
        key: 'characterName',
        renderer: this.characterNameRenderer,
        style: 'xl compact'
    }, {
        title: <T value='pilots.shipType'/>,
        key: 'shipType',
        style: 'm compact'
    }, {
        title: <T value='pilots.location'/>,
        key: 'location',
        style: 'l compact'
    }, {
        title: <T value='pilots.joined'/>,
        key: 'startDateTime',
        renderer: this.startDateTimeRenderer,
        style: 's tac compact',
        invertDefaultSort: true
    }, {
        title: <T value='pilots.login'/>,
        key: 'logonDateTime',
        renderer: this.logonDateTimeRenderer,
        style: 's tac compact',
        invertDefaultSort: true
    }, {
        title: <T value='pilots.kills'/>,
        key: 'kills',
        style: 'xs tac',
        invertDefaultSort: true
    }, {
        title: <T value='pilots.losses'/>,
        key: 'losses',
        style: 'xs tac',
        invertDefaultSort: true
    }, {
        title: <T value='pilots.points'/>,
        key: 'points',
        style: 'xs tac',
        invertDefaultSort: true
    }, {
        title: <T value='pilots.income'/>,
        key: 'income',
        renderer: this.incomeRenderer,
        style: 's tac',
        invertDefaultSort: true
    }, {
        actions: [{
            text: <T value='pilots.actions.comment'/>,
            key: 'addComment',
            action: record => {
                console.log(record);
            }
        }]
    }];

    render() {
        const pilots = this.props.pilots;
        return (
            <Grid
                name='pilots'
                list={pilots}
                pageSize={16}
                searchFields={['characterName', 'shipType', 'location']}
                columns={this.columns}
                uniqueField="characterID"
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        pilots: state.subs.pilots
    };
}

export default connect(mapStateToProps)(Pilots);