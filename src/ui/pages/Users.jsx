import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Translate as T} from 'react-redux-i18n';
import {Button} from 'react-bootstrap';

import {Grid} from 'ui/components/Grid';
import CharacterName from 'ui/components/CharacterName';
import Collection from 'ui/components/Collection';
import {populate} from 'client/collections';
import {subscribe} from 'client/subscriber';

import {addToGroup, removeFromGroup, removeUser} from 'state/users/actions';

import '../styles/style.styl';

class Users extends Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        users: PropTypes.array,
        groups: PropTypes.array
    };

    // noinspection JSUnusedGlobalSymbols
    static defaultProps = {
        users: null,
        groups: []
    };

    constructor() {
        super();
    }

    componentDidMount() {
        subscribe('groups');
        subscribe('users');
    }

    addGroup = (data, groupID) => {
        this.props.dispatch(addToGroup(data.characterID, groupID));
    };

    removeGroup = (data, groupID) => {
        this.props.dispatch(removeFromGroup(data.characterID, groupID));
    };

    characterNameRenderer = ({data}) => <CharacterName
        characterName={data.characterName}
        characterID={data.characterID}
    />;

    groupsRenderer = ({val, data}) => (
        <Collection
            current={populate(val, this.props.groups)}
            total={this.props.groups}
            addAction={this.addGroup}
            removeAction={this.removeGroup}
            sorter={(a, b) => (a.name > b.name)}
            idKey="_id"
            displayKey="name"
            data={data}
        />
    );

    groupsSorter = (a, b) => {
        a = JSON.stringify(a);
        b = JSON.stringify(b);
        if (a > b)
            return 1;
        else if (a < b)
            return -1;
        return 0;

    };

    _removeUser = user => this.props.dispatch(removeUser(user.characterID));

    columns = [{
        title: <T value='users.single'/>,
        key: 'characterName',
        style: 'l',
        renderer: this.characterNameRenderer
    }, {
        title: <T value='groups.plural'/>,
        key: 'groups',
        style: 'xl',
        renderer: this.groupsRenderer,
        sorter: this.groupsSorter
    }, {
        actions: [{
            text: <span><i className="fa fa-times"/> <T value='users.remove'/></span>,
            key: 'removeUser',
            action: data => {
                this._removeUser(data);
            }
        }]
    }];

    render() {
        return (
            <div>
                <Grid
                    name='users'
                    list={this.props.users}
                    pageSize={16}
                    searchFields={['characterName', 'groups']}
                    columns={this.columns}
                    uniqueField="characterID"
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        users: state.subs.users,
        groups: state.subs.groups
    };
}

export default connect(mapStateToProps)(Users);