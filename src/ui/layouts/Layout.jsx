import React from 'react';
import {Grid} from 'react-bootstrap';

import Header from 'ui/components/Header';
import Notifications from 'ui/components/Notifications';

import '../styles/bootstrap.css';
import '../styles/style.styl';

const App = (props) => (
    <div>
        <Header/>
        <Grid>
            {props.children}
        </Grid>
        <Notifications/>
    </div>
);

export default App;