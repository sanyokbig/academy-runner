import connectMongo from 'connect-mongo';
import path from 'path';
import React from 'react';
import express from 'express';
import Session from 'express-session';
import SharedSession from 'express-socket.io-session';
import expressStaticGzip from 'express-static-gzip';
import {renderToString} from 'react-dom/server';
import {Provider} from 'react-redux';
import {StaticRouter} from 'react-router-dom';

import socketServer from 'server/socketServer';
import App from './ui/App';
import mongoose from 'server/mongoose';
import Logger from 'server/modules/Logger';
import SocketListener from 'server/modules/SocketListener';
import AssetManager from 'universal/AssetManager';
import config from 'server/config';
import AuthCallbackResponder from 'server/modules/AuthCallbackResponder';
import AuthStatePasser from 'server/modules/AuthStatePasser';
import 'server/publications';
import 'server/schedule';
import 'server/cleanDrafts';
import './translations';
import 'server/overrideMomentLocales';

import {prepareStore} from 'state/prepareStore';

const logger = new Logger('Server');

// Нормальное сообщение о необработанном реджекте
process.on('unhandledRejection', r => logger.error(r));

const app = express();

const mongoStore = new (connectMongo(Session))(({
    mongooseConnection: mongoose.connection,
    stringify: false
}));

const session = Session({
    resave: false,
    saveUninitialized: true,
    store: mongoStore,
    secret: config.session.secret
});

const staticPath = process.env.PROD ? 'client' : 'static';

app
    .use('/dist/', expressStaticGzip(path.resolve(__dirname, staticPath)))
    .use(session)

    // Прием ответов от сервера аутентификации
    .use(config.eveAuth.callbackRoute, (...args) => new AuthCallbackResponder(...args))

    //Запись статуса аутентификации в запрос.
    .use((...args) => new AuthStatePasser(...args))

    // Генерация сторы
    .use(prepareStore)

    // Генерация приложения
    .use((req, res) => {
        const store = req.preparedStore;

        const context = {};

        //Серверный рендеринг
        const componentHTML = renderToString(
            <Provider store={store}>
                <StaticRouter
                    location={req.url}
                    context={context}
                >
                    <App/>
                </StaticRouter>
            </Provider>
        );

        return res.end(renderHTML(componentHTML, store.getState()));
    });

const clientConfig = {
    grid: {
        defaultGridPageSize: config.grid.defaultGridPageSize
    },
    ntf: {
        max: config.ntf.max,
        lifetime: config.ntf.lifetime
    },
    server: {
        host: config.server.host,
        port: config.server.port
    },
    socket: {
        uri: 'http://' + config.server.host + ':' + config.server.port
    }
};

function renderHTML(componentHTML, initialState) {
    return `
    <!DOCTYPE html>
      <html>
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Academy Runner</title>
          <link rel="stylesheet" href="${AssetManager.get('bundle.css')}">
          <link rel="shortcut icon" type="image/x-icon" href="${AssetManager.get('favicon.ico')}" />
      </head>
      <body>
        <div id="react-view">${componentHTML}</div>
        <div id="dev-tools"></div>
        <script>
            window.__INITIAL_STATE__= ${JSON.stringify(initialState)};
            window.__ASSETS__= ${JSON.stringify(AssetManager.assets)};
            window.__CONFIG__ = ${JSON.stringify(clientConfig)};
        </script>
        <script type="application/javascript" src="${AssetManager.get('bundle.js')}"></script>
      </body>
    </html>
  `;
}

let server = app.listen(config.server.port, () => {
    logger.info('Server listening on:', config.server.port);
});

socketServer.listen(server);
socketServer.use(SharedSession(session));
socketServer.sockets.on('connect', (socket) => {
    const socketListener = new SocketListener(socket);

    socket.once('disconnect', () => {
        socketListener.disconnected(socket);
    });
});