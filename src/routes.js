import React from 'react';

import Home from './ui/pages/Home';
import Pilots from './ui/pages/Pilots';
import Incomes from './ui/pages/Incomes';
import Users from './ui/pages/Users';
import Groups from './ui/pages/Groups';

import {Switch, Route} from 'react-router-dom';
import PrivateRoute from './ui/components/PrivateRoute/index';

const routes =() => (
    <Switch>
        <Route path="/" exact component={Home}/>
        <PrivateRoute path="/pilots" exact component={Pilots} require='pilotsRead'/>
        <PrivateRoute path="/incomes" exact component={Incomes} require='incomesRead'/>
        <PrivateRoute path="/users" exact component={Users} require="usersRead"/>
        <PrivateRoute path="/groups" exact component={Groups} require="groupsRead"/>
    </Switch>
);

export default routes;