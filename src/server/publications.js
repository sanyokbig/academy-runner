import moment from 'moment';

import publisher from './publisher';
import Utils from './modules/Utils';
import 'models/User';
import 'models/Group';
import 'models/Income';

publisher
    .publish('users', 'user', {
        require: 'usersRead',
        uniqueField: 'characterID',
        fields: {_id: 0, characterName: 1, characterID: 1, groups: 1, _draft: 1}
    })
    .publish('pilots', 'user', {
        require: 'pilotsRead',
        query: {groups: 'pilot', _draft: null},
        prevQuery: {_groups: 'pilot'},
        uniqueField: 'characterID',
        fields: {_id: 0, api: 0, groups: 0},
        transform: doc => {
            doc.timeInCorp = moment.utc(doc.startDateTime).locale('ru').from(moment.utc());
            doc.startDateTime = moment.utc(doc.startDateTime).format('YYYY-MM-DD HH:mm');

            doc.sinceLastLogin = moment.utc(doc.logonDateTime).locale('ru').from(moment.utc());
            doc.logonDateTime = moment.utc(doc.logonDateTime).format('YYYY-MM-DD HH:mm');

            // Приведение дохода к более читаемому виду
            doc.income && (doc.simplifiedIncome = Utils.simplifyNumber(doc.income));

            return doc;
        }
    })
    .publish('groups', 'group', {
        require: 'groupsRead',
        fields: {__v: 0}
    })
    .publish('incomes', 'income', {
        require: 'incomesRead',
        fields: {__v: 0},
        populate: {
            path:'character',
            select: 'characterName'
        },
        transform: doc => {
            doc.readableDate = moment.utc(doc.date).format('YYYY-MM-DD HH:mm');
            doc.characterName = doc.character.characterName;

            doc.iskified = Utils.iskify(doc.amount);

            delete doc.id;
            delete doc.character;

            return doc;
        }
    });