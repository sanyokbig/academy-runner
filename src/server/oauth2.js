import simpleOAuth2 from 'simple-oauth2';

import config from 'server/config';

export default simpleOAuth2.create({
    client: {
        id: config.eveAuth.id,
        secret: config.eveAuth.secret
    },
    auth: {
        tokenHost: config.eveAuth.tokenHost
    }
});