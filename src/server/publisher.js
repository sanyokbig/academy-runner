import Publisher from './modules/Publisher';

const publisher = new Publisher();

export default publisher;