import publisher from '../publisher';
import {initialGridState} from 'state/grid/reducer';

// Предзагрузка данных для страницы с группами
export const incomes = async (match, sessionID) => {
    const data = {
        subs: {},
        grid: {}
    };

    // Подписка на группы
    data.subs = await publisher.serverSideSubscription(sessionID, ['incomes']);
    data.grid.incomes = initialGridState();

    return data;
};