import publisher from '../publisher';
import {initialGridState} from 'state/grid/reducer';

// Предзагрузка данных для страницы с пилотами
export const pilots = async (match, sessionID) => {
    const data = {
        subs: {},
        grid: {}
    };

    // Подписка на пилотов
    data.subs = await publisher.serverSideSubscription(sessionID, ['pilots']);
    data.grid.pilots = initialGridState({searchFields: ['characterName', 'shipType', 'location']});

    return data;
};