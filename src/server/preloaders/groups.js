import publisher from '../publisher';
import {initialGridState} from 'state/grid/reducer';

// Предзагрузка данных для страницы с группами
export const groups = async (match, sessionID) => {
    const data = {
        subs: {},
        grid: {}
    };

    // Подписка на группы
    data.subs = await publisher.serverSideSubscription(sessionID, ['groups']);
    data.grid.groups = initialGridState();

    return data;
};