import publisher from '../publisher';
import {initialGridState} from 'state/grid/reducer';

// Предзагрузка данных для страницы с юзерами
export const users = async (match, sessionID) => {
    const data = {
        subs: {},
        grid: {}
    };

    // Подписка на юзеров и группы
    data.subs = await publisher.serverSideSubscription(sessionID, ['users','groups']);
    data.grid.users = initialGridState({searchFields: ['characterName', 'groups']});

    return data;
};