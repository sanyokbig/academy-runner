import yaml from 'js-yaml';
import fs from 'fs';
import path from 'path';

import Logger from 'server/modules/Logger';

const logger = new Logger('Config');

const configPath = process.env.PROD ? './config.yaml' : '../config.yaml';

let resolvedPath = path.resolve(__dirname, configPath),
    config;

logger.info('Loading config from', resolvedPath);

try {
    config = yaml.safeLoad(fs.readFileSync(resolvedPath), 'utf-8');
} catch (e) {
    logger.alert('File read failed', e);
}

module.exports = config;