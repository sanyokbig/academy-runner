import moment from 'moment';

import Logger from './Logger';
import Kill from 'models/Kill';
import User from 'models/User';

const logger = new Logger('PvpStats');

export default class PvpStats {
    stats = [];
    DEFAULT_STAT = {
        kills: 0,
        losses: 0,
        points: 0
    };

    // Если первый аргумент число, отнимает от текущей даты переданное количество дней и использует как начальную
    async update(startTime, endTime = moment.utc().toDate()) {
        if (typeof startTime === 'number') {
            startTime = moment.utc().subtract(startTime, 'days').toDate();
        }

        await this.generate(startTime, endTime);
        await this.apply();
    }

    async generate(startTime, endTime) {
        logger.info('Generating PVP stats between', moment.utc(startTime).format('YYYY-MM-DD HH:mm'), 'and', moment.utc(endTime).format('YYYY-MM-DD HH:mm'));

        // Получаем все киллы за заданный период
        const kills = await Kill
            .find()
            .where('date').gt(startTime).lt(endTime);

        for (let kill of kills) {
            // Проходимся по участникам килла
            kill.involved.map(id => {
                // Ищем данные пилота в результатах, если их нет, создаем новые
                let pilot = this.stats.find(data => data.id === id) || (this.stats[this.stats.length] = pilot = {
                    id: +id,
                    ...this.DEFAULT_STAT
                });

                if (kill.type === 'kill') {
                    pilot.kills++;
                    pilot.points += kill.points;
                } else {
                    pilot.losses++;
                    pilot.points -= kill.points;
                }
            });
        }

        logger.info('PVP stats generated');
    }

    async apply() {
        logger.info('Applying PVP stats');

        for (const pilot of await User.get()){
            let stat = this.stats.find(_s => _s.id === pilot.characterID);

            if(stat) {
                pilot.set(stat);
            } else {
                pilot.set(this.DEFAULT_STAT);
            }

            pilot.isModified() && await pilot.save();
        }

        logger.result('PVP stats applied');
    }
}