import mongoose from 'server/mongoose';
import Logger from './Logger';

const logger = new Logger('DraftCleaner');

export default class DraftCleaner {
    clean(modelsToClean) {
        modelsToClean.map(async model => {
            const drafted = await mongoose.model(model).get({
                _draft: {$ne: null}
            });

            drafted.map(async doc => {
                doc.set({_draft: null});
                try {
                    await doc.save();
                } catch (e) {
                    logger.error(e);
                }
            });
        });
    }
}
