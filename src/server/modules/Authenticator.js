import Session from 'models/Session';
import Logger from 'server/modules/Logger';
import socketServer from 'server/socketServer';
import {AUTH_STATE} from 'state/auth/actions';

const logger = new Logger('Authenticator');

export default class Authenticator {
    constructor(sessionID) {
        this.sessionID = sessionID;
    }

    async authorize(user) {
        if (!user) {
            logger.error('Authorization aborted: no user');
            throw new Error('no user passed');
        }

        //Ищем переданую сессию
        this.session = await Session.get(this.sessionID);

        if (!this.session) {
            //Сессия не найдена, логин не прошел
            logger.error('Session not found:', this.sessionID);
            throw new Error('session not found');
        }

        //Сессия найдена, записываем в нее юзера
        try {
            await this.session.login(user._id, await user.getRoles(user._id));
        } catch (e) {
            logger.error(e);
            throw new Error('session login failed');
        }

        logger.result('Successfully logged with user', user._id, 'to session', this.sessionID);
    }

    async loginSockets(){
        for (let socketId of this.session.session.sockets) {
            const authState = await this.session.getAuthState();

            socketServer.sockets.to(socketId).emit('action', {
                type: AUTH_STATE,
                data: authState,
                ntf: {
                    value: 'auth.loginSuccess',
                    name: authState.user.characterName
                }
            });
        }
    }
}