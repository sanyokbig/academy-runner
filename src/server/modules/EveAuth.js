import oauth2 from 'server/oauth2';
import Logger from 'server/modules/Logger';

const logger = new Logger('EveAuth');

export default class EveAuth {
    // Обновляет токен у читалки, сохраняет и возвращает
    static async refreshReaderToken(reader) {
        const tokenObject = reader.api,
            token = oauth2.accessToken.create(tokenObject);

        let result;
        try {
            result = await token.refresh();
        } catch (e) {
            logger.error(e);
            return null;
        }
        Object.assign(reader.api, result.token);

        try {
            return await reader.save();
        } catch (e) {
            logger.error(e);
            return null;
        }
    }
}


