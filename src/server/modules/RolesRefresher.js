import Logger from 'server/modules/Logger';
import mongoose from 'server/mongoose';
import User from 'models/User';
import Session from 'models/Session';

const logger = new Logger('RolesRefresher');

export default class RolesRefresher {
    constructor(model, doc){
        this.model = model;
        this.doc= doc;
    }

    async refresh(){
        logger.info('Auth rules changed, refreshing roles');

        // Документ уже в обработке, обнуляем поле
        mongoose.model(this.model).findOneAndUpdate({_id: this.doc._id}, {$set: {_authRulesChanged: false}});

        // Генерация списка юзеров, которых затронуло изменение
        const affected = [];

        if (this.model === 'user') {
            // Обновлен юзер, записываем его
            affected.push(this.doc._id);
        } else if (this.model === 'group') {
            // Обновлена группа, ищем всех юзеров группы
            let users = [];
            try {
                users = await User.get({groups: this.doc._id});
            } catch (e) {
                logger.alert(e);
            }
            users.map(user => affected.push(user._id));
        }

        // Получение сессий, которые надо обновить
        let sessions;
        try {
            sessions = await Session
                .find()
                .where('session.loggedAs').in(affected) // Залогиненные сессии
                .where('session.sockets').ne([])// С активными сокетами
                .populate('session.loggedAs')
                .exists('session.sockets');
        } catch (e) {
            logger.error(e);
            sessions = [];
        }


        // Проход по сессиям, генерация нового списка ролей
        for (let session of sessions) {
            // Записывать новый стейт в сессию
            session.session.roles = await session.session.loggedAs.getRoles(session.session.loggedAs);
            // Сохранять, отправлять на фронт
            try {
                await session.save();
            } catch (e) {
                logger.error(e);
            }
        }

        return sessions;
    }
}