import schedule from 'node-schedule';

import Logger from 'server/modules/Logger';

const logger = new Logger('Scheduler');

export default class Scheduler {
    jobs = {};

    add(job, cronConfig, action, runImmediately = false) {
        logger.info('Adding job', job);
        this.jobs[job] = schedule.scheduleJob(cronConfig, () => {
            logger.info('Executing job', job);
            action();
        });

        if(runImmediately) {
            logger.info('Executing job', job, 'immediately');
            action();
        }

        return this;
    }
}
