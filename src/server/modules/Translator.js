import yaml from 'js-yaml';
import fs from 'fs';
import path from 'path';

import Logger from 'server/modules/Logger';

const translationsPath = process.env.PROD ? './translations' : '../../translations';

const logger = new Logger('Translator');

export default class Translator {
    static getTranslations(langs) {
        const translations = {};
        let doc, resolvedPath;

        for (let lng of langs) {
            resolvedPath = path.resolve(__dirname, translationsPath, lng + '.yaml');
            logger.info('Loading', lng, 'translations from', resolvedPath);
            try {
                doc = yaml.safeLoad(fs.readFileSync(resolvedPath), 'utf-8');
                translations[lng] = doc;
            } catch (e) {
                logger.alert('File read failed', e);
            }
        }

        return translations;
    }

    static pluralize(data) {
        if (typeof(data.count) === 'undefined') {
            return data;
        }
        const n = data.count;
        if (!n) {
            data.value += '.zero';
        } else if ((n % 10) === 1 && (n % 100 !== 11)) {
            data.value += '.one';
        } else if ([2, 3, 4].includes(n % 10) && !([12, 13, 14].includes(n % 100))) {
            data.value += '.few';
        } else if (!(n % 10) || ![5, 6, 7, 8, 9].includes(n % 10) || ![11, 12, 13, 14].includes(n % 100)) {
            data.value += '.many';
        }
        return data;
    }
}