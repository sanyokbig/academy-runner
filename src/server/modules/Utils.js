export default class Utils {
    static simplifyNumber(number) {
        typeof number === 'string' && (number = +number);

        if (typeof number !== 'number' || isNaN(number)) {
            throw new Error(number + ' is not a number');
        }

        let thousands = 0;

        while (number >= 1000) {
            number = Math.round(number/1000);
            thousands++;
        }

        return '' + number + 'k'.repeat(thousands);
    }

    static iskify(string) {
        typeof string === 'number' && (string = '' + string);

        if (typeof string !== 'string' || isNaN(string)) {
            throw new Error(string + ' is not a number');
        }

        return string.replace(/\B(?=(\d{3})+(?!\d))/g, ' ') + ' isk';
    }
}