import moment from 'moment';

import Logger from './Logger';
import Income from 'models/Income';
import User from 'models/User';

const logger = new Logger('PvpStats');

export default class IncomeStats {
    stats = [];
    DEFAULT_STAT = {
        income: 0
    };

    async update(startTime, endTime = moment.utc().toDate()) {
        if (typeof startTime === 'number') {
            startTime = moment.utc().subtract(startTime, 'days').toDate();
        }

        await this.generate(startTime, endTime);
        await this.apply();
    }

    async generate(startTime, endTime = moment.utc().toDate()) {
        logger.info('Generating Income stats between', moment.utc(startTime).format('YYYY-MM-DD HH:mm'), 'and', moment.utc(endTime).format('YYYY-MM-DD HH:mm'));
        
        // Ищем доходы за переданный период
        const incomes = await Income
            .find()
            .where('date').gt(startTime).lt(endTime);
        

        for (let income of incomes) {
            // Ищем данные пилота в результатах, если их нет, создаем новые
            let pilot = this.stats.find(data => data.characterID === income.characterID) || (this.stats[this.stats.length] = pilot = {
                characterID: income.characterID,
                amount: 0
            });

            pilot.amount += income.amount;
        }

        logger.info('Income stats generated');
    }

    async apply() {
        logger.info('Applying Income stats');

        for (const pilot of await User.get()){
            let stat = this.stats.find(_s => _s.characterID === pilot.characterID);

            if(stat) {
                pilot.set({
                    income: stat.amount
                });
            } else {
                pilot.set(this.DEFAULT_STAT);
            }

            pilot.isModified() && await pilot.save();
        }

        logger.result('Income stats applied');
    }
}