describe('Authenticator', () => {
    describe('#getTokenData', () => {
        it('uses stored authorization code');
        it('obtains access and refresh tokens from ccp');
        it('throws if obtain fails');
    });

    describe('#getUserData', () => {
        it('uses stored token data');
        it('calls token verification');
        it('returns player info');
        it('throws on request error');
    });

    describe('#authorize', () => {
        it('looks for existing tokens for stored user');
    });
});
