import moment from 'moment';
import User from 'models/User';
import Logger from './Logger';

const logger = new Logger('PilotsProcessor');

export default class PilotsProcessor {
    // Переводить поля в ютс формат
    dateFields = ['startDateTime', 'logonDateTime', 'logoffDateTime'];
    // Не приписывать к пилотам, есди состоит в одной из групп
    ignoredGroups = ['admin', 'instructor', 'reader'];

    // Обработка полученных по апи пилотов
    async process(pilots) {
        const processing = [];
        for (const pilot of pilots) {
            processing.push(this.processSingle(pilot));
        }
        const results = await Promise.all(processing);

        logger.result('Processed', pilots.length, 'pilots:',
            results.filter(s => s === 'added').length, 'added,',
            results.filter(s => s === 'updated').length, 'updated,',
            results.filter(s => !s).length, 'failed');
    }

    // Подготовка объекта для записи в базу
    prepare(pilot) {
        const prepared = Object.assign({}, pilot);

        // Имя хранится в name, переносим в characterName
        prepared.characterName = pilot.name;
        delete prepared.name;

        // Переводим даты в utc
        this.dateFields.map(_f => prepared[_f] = moment.utc(prepared[_f]).toDate());

        return prepared;
    }

    // Обработка пилота
    async processSingle(pilot) {
        // Подготовка объекта
        const pilotData = this.prepare(pilot);

        // Поиск юзера пилота
        let user = await User.getOne({'characterID': +pilotData.characterID});

        let isNew;
        if (user) {
            // Юзер найден, обновляем
            user.set(pilotData);

            // Заносим в пилоты, если не в группе админ/инструктор
            if (user.groups) {
                // Убираем из бывших, если он там был
                user.groups = user.groups.filter(group => group !== 'expilot');
                // Если у юзера не стоит группа пилот и он не является системным юзером, выставить пилота
                !user.groups.includes('pilot')
                && !user.groups.filter(group => this.ignoredGroups.includes(group)).length
                && user.groups.push('pilot');
            } else {
                user.groups = ['pilot'];
            }
        } else {
            isNew = true;
            // Юзер не найден, создаем нового пилота
            user = new User({
                ...pilotData,
                groups: ['pilot']
            });
        }

        // Сохраняем
        try {
            user.isModified() && await user.save();
            return isNew ? 'added' : 'updated';
        } catch (e) {
            logger.error(e);
            return false;
        }
    }
}