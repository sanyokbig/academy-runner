import Logger from 'server/modules/Logger';
import Token from 'models/Token';
import PilotsReader from './PilotsReader';
import PilotsProcessor from './PilotsProcessor';
import PilotsCleaner from './PilotsCleaner';

const logger = new Logger('PilotsUpdater');

export default class PilotsUpdater {
    async run() {
        logger.result('Pilots update began');
        const tokens = await Token.get({scopes: 'corporationMembersRead'});

        for (let token of tokens) {
            const pilotsReader = new PilotsReader(token),
                pilotsProcessor = new PilotsProcessor(),
                pilotsCleaner = new PilotsCleaner();

            try {
                await pilotsReader.read();
                await pilotsProcessor.process(pilotsReader.pilots);
                await pilotsCleaner.clean(pilotsReader.pilots.map(_p => +_p.characterID));
            } catch (e) {
                logger.error(e.message);
            }
        }
        logger.result('Pilots update done');
    }
}