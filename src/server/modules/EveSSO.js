import request from 'request-promise';

import Logger from 'server/modules/Logger';
import oauth2 from 'server/oauth2';

const logger = new Logger('EveSSO');

export default class EveSSO {
    tokenData = null;
    userData = null;

    constructor(code) {
        this.code = code;
    }

    async getTokenData() {
        try {
            return this.tokenData = await oauth2.authorizationCode.getToken({code: this.code});
        } catch (e) {
            logger.error(e);
            throw new Error('token obtain failed');
        }
    }

    async verify() {
        let verified;
        try {
            verified = JSON.parse(await request.get({
                url: 'https://login.eveonline.com/oauth/verify',
                headers: {
                    'Authorization': 'Bearer ' + this.tokenData.access_token
                }
            }));
        } catch (e) {
            logger.error(e);
            throw new Error('token verification failed');
        }

        this.userData = {
            characterID: verified.CharacterID,
            characterName: verified.CharacterName
        };

        Object.assign(this.tokenData, {
            Scopes: verified.Scopes,
            TokenType: verified.TokenType
        });
    }
}