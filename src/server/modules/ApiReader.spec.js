import chai, {expect, assert} from 'chai';
import chaiAsPromised from 'chai-as-promised';
import sinon from 'sinon';
import moment from 'moment';
import request from 'request-promise';

import ApiReader from './ApiReader';
import Logger from './Logger';
import oauth2 from 'server/oauth2';

chai.use(chaiAsPromised);

describe('ApiReader', () => {
    const sandbox = sinon.sandbox.create();
    let expiredToken = {
            access_token: 'old-access-token',
            expires_at: moment.utc('2017-08-08 14:50:00').toDate(),
            save: () => null
        },
        actualToken = {
            access_token: 'old-access-token',
            expires_at: moment.utc('2017-08-08 15:10:00').toDate(),
            save: () => null
        };

    before(() => {
        sandbox.useFakeTimers(moment.utc('2017-08-08 15:00:00').toDate());
        sandbox.stub(Logger, 'openLog');
        sandbox.stub(Logger.prototype);
    });

    after(() => {
        sandbox.restore();
    });

    describe('#isExpired', () => {
        it('determines if token expired', () => {
            const expiredReader = new ApiReader(expiredToken),
                exactlyExpiredReader = new ApiReader({expires_at: moment.utc('2017-08-08 15:00:00').toDate()}),
                actualReader = new ApiReader(actualToken);

            expect(expiredReader.isExpired()).to.equal(true);
            expect(exactlyExpiredReader.isExpired()).to.equal(true);
            expect(actualReader.isExpired()).to.equal(false);
        });
    });

    describe('#refresh', () => {
        describe('when token and mongo works', () => {
            const refreshSandbox = sinon.sandbox.create();

            let dumbToken;

            beforeEach(() => {
                dumbToken = {
                    access_token: 'old-access-token',
                    expires_at: moment.utc('2017-08-08 14:50:00').toDate(),
                    save: () => null
                };

                refreshSandbox.stub(dumbToken, 'save').returns();
                refreshSandbox.stub(oauth2.accessToken, 'create').returns({
                    refresh: async () => ({
                        token: {
                            access_token: 'new-access-token',
                            expires_at: moment.utc('2017-08-08 17:00:00').toDate()
                        }
                    })
                });
            });

            afterEach(() => {
                refreshSandbox.restore();
            });

            it('refreshes expired token', async () => {
                refreshSandbox.stub(ApiReader.prototype, 'isExpired').returns(true);

                const apiReader = new ApiReader(dumbToken);
                await apiReader.refresh();

                expect(apiReader.token.access_token).to.be.equal('new-access-token');
            });

            it('respects expiration', async () => {
                refreshSandbox.stub(ApiReader.prototype, 'isExpired').returns(false);

                const apiReader = new ApiReader(dumbToken);
                await apiReader.refresh();

                expect(apiReader.token.access_token).to.be.equal('old-access-token');
            });

            it('can ignore expire', async () => {
                refreshSandbox.stub(ApiReader.prototype, 'isExpired').returns(false);

                const apiReader = new ApiReader(dumbToken);
                await apiReader.refresh(true);

                expect(apiReader.token.access_token).to.be.equal('new-access-token');
            });

            it('saves token', async () => {
                refreshSandbox.stub(ApiReader.prototype, 'isExpired').returns(true);

                const apiReader = new ApiReader(dumbToken);
                await apiReader.refresh();

                sinon.assert.calledOnce(dumbToken.save);
            });
        });

        describe('when token refresh fails', () => {
            const refreshSandbox = sinon.sandbox.create();

            let dumbToken;

            beforeEach(() => {
                dumbToken = {
                    access_token: 'old-access-token',
                    expires_at: moment.utc('2017-08-08 14:50:00').toDate(),
                    save: () => null
                };

                refreshSandbox.stub(ApiReader.prototype, 'isExpired').returns(true);

                refreshSandbox.stub(dumbToken, 'save').returns();

                refreshSandbox.stub(oauth2.accessToken, 'create').returns({
                    refresh: async () => {
                        throw new Error();
                    }
                });
            });
            afterEach(() => {
                refreshSandbox.restore();
            });

            it('throw an token refresh failed error', async () => {
                const apiReader = new ApiReader(dumbToken);

                return assert.isRejected(apiReader.refresh(), 'token refresh failed');
            });
        });

        describe('when token save fails', () => {
            const refreshSandbox = sinon.sandbox.create();

            let dumbToken;

            beforeEach(() => {
                dumbToken = {
                    access_token: 'old-access-token',
                    expires_at: moment.utc('2017-08-08 14:50:00').toDate(),
                    save: () => null
                };

                refreshSandbox.stub(ApiReader.prototype, 'isExpired').returns(true);

                refreshSandbox.stub(dumbToken, 'save').throws();

                refreshSandbox.stub(oauth2.accessToken, 'create').returns({
                    refresh: async () => ({
                        token: {
                            access_token: 'new-access-token',
                            expires_at: moment.utc('2017-08-08 17:00:00').toDate()
                        }
                    })
                });
            });

            afterEach(() => {
                refreshSandbox.restore();
            });

            it('throw an token refresh failed error', async () => {
                const apiReader = new ApiReader(dumbToken);

                return assert.isRejected(apiReader.refresh(), 'token save failed');
            });
        });
    });

    describe('#useESI', () => {
        let esiSandbox;

        const shortToken = {access_token: 'use me'};

        beforeEach(() => {
            esiSandbox = sinon.createSandbox();
        });

        afterEach(() => {
            esiSandbox.restore();
        });

        it('requests to ccp esi endpoint', async () => {
            const req = esiSandbox.stub(request, 'get').returns(Promise.resolve('{"ok": 1}'));

            await (new ApiReader(shortToken)).useESI('some/endpoint');

            sinon.assert.calledWith(req, sinon.match({
                url: 'https://esi.tech.ccp.is/latest/some/endpoint'
            }));
        });

        it('passes stored token to request', async () => {
            const req = esiSandbox.stub(request, 'get').returns(Promise.resolve('{"ok": 1}'));

            const apiReader = new ApiReader(shortToken);

            await apiReader.useESI('some/endpoint');

            sinon.assert.calledWith(req, sinon.match({
                token: shortToken.access_token
            }));
        });

        it('returns a parsed object', () => {
            const apiReader = new ApiReader(shortToken);

            esiSandbox.stub(request, 'get').returns('{"character": "Alexander"}');

            const pr = apiReader.useESI('ep');

            return Promise.all([
                assert.becomes(pr, {character: 'Alexander'}),
                assert.eventually.typeOf(pr, 'object')
            ]);
        });

        it('throws on call without endpoint', async () => {
            const apiReader = new ApiReader(shortToken);

            return assert.isRejected(apiReader.useESI(), 'endpoint not specified');
        });

        it('throws on request error', () => {
            const apiReader = new ApiReader(shortToken);

            esiSandbox.stub(request, 'get').throws();

            return assert.isRejected(apiReader.useESI('endpoint'), 'request error');
        });

        it('throw on parse error', () => {
            const apiReader = new ApiReader(shortToken);

            esiSandbox.stub(request, 'get').returns('unparseable');

            return assert.isRejected(apiReader.useESI('endpoint'), 'parse error');
        });
    });

    describe('#useXML', () => {
        let xmlSandbox, requestStub;

        const xml = `<eveapi version="2">
    <currentTime>2016-01-10 19:02:13</currentTime>
    <result>
        <rowset name="members">
            <row characterID="1"/>
            <row characterID="2" />
        </rowset>
    </result>
    <cachedUntil>2016-01-10 23:23:18</cachedUntil>
</eveapi>`,
            xmlParsed = {
                name: 'members',
                row: [{characterID: '1'}, {characterID: '2'}]
            };

        const multiRowsetXml = `<eveapi version="2">
                <currentTime>2016-01-10 19:02:13</currentTime>
                <result>
                    <rowset name="members">
                        <row characterID="55"/>
                        <row characterID="56" />
                    </rowset>
                    <rowset name="corps">
                        <row corpID="13"/>
                        <row corpID="12" />
                    </rowset>
                    <rowset name="alis">
                        <row alianceID="1"/>
                        <row alianceID="2" />
                    </rowset>
                </result>
                <cachedUntil>2016-01-10 23:23:18</cachedUntil>
            </eveapi>`,
            multiRowsetXmlParsed = [{
                name: 'members',
                row: [{characterID: '55'}, {characterID: '56'}]
            }, {
                name: 'corps',
                row: [{corpID: '13'}, {corpID: '12'}]
            }, {
                name: 'alis',
                row: [{alianceID: '1'}, {alianceID: '2'}]
            }];

        const shortToken = {access_token: 'use me'};

        beforeEach(() => {
            xmlSandbox = sinon.createSandbox();
            requestStub = xmlSandbox.stub(request, 'get');
        });

        afterEach(() => {
            xmlSandbox.restore();
        });

        it('requests to ccp xml path', async () => {
            requestStub.returns(Promise.resolve(xml));

            const apiReader = new ApiReader(shortToken);

            await apiReader.useXML('some-path');

            sinon.assert.calledWith(requestStub, sinon.match({
                url: 'https://api.eveonline.com/some-path.xml.aspx',
            }));
        });

        it('passes stored token to request', async () => {
            requestStub.returns(Promise.resolve(xml));

            const apiReader = new ApiReader(shortToken);
            await apiReader.useXML('some-path');

            sinon.assert.calledWith(requestStub, sinon.match({qs: {accessToken: shortToken.access_token}}));
        });

        it('returned rowset can be object or array', async () => {
            requestStub.callsFake(({url}) =>
                url === 'https://api.eveonline.com/simple.xml.aspx'
                    ? Promise.resolve(xml)
                    : Promise.resolve(multiRowsetXml)
            );

            const apiReader = new ApiReader(shortToken);

            const _simple = await apiReader.useXML('simple'),
                _multi = await apiReader.useXML('multi');

            assert.typeOf(_simple, 'object');
            assert.deepEqual(_simple, xmlParsed);

            assert.typeOf(_multi, 'array');
            assert.deepEqual(_multi, multiRowsetXmlParsed);
        });

        it('throws on call without path', () => {
            const apiReader = new ApiReader(shortToken);

            return assert.isRejected(apiReader.useXML(), 'path not specified');
        });

        it('throws on request error', () => {
            const apiReader = new ApiReader(shortToken);

            requestStub.throws();

            return assert.isRejected(apiReader.useXML('some-path'), 'request error');
        });

        it('throws on xml parse error', () => {
            const apiReader = new ApiReader(shortToken);

            requestStub.returns(Promise.resolve('unparseable'));

            return assert.isRejected(apiReader.useXML('some-path'), 'xml parse error');
        });
    });
});
