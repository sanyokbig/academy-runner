import {assert} from 'chai';

import Utils from './Utils';

describe('Utils', () => {
    const tiny = 315,
        small = 774301,
        average = 14164332,
        epic = 56400112500,
        string = '51230445',
        unacceptable = 'wow such NaN';

    describe('simplifyNumber', () => {
        it('returns a string', () => {
            assert.typeOf(Utils.simplifyNumber(average), 'string');
        });
        it('accepts number or string',() => {
            assert(Utils.simplifyNumber(average));
            assert(Utils.simplifyNumber(string));
        });
        it('throws if can\'t use argument', () => {
            assert.throws(() => Utils.simplifyNumber(unacceptable));
        });
        it('do not changes numbers less then 1000',() => {
            assert.equal(Utils.simplifyNumber(tiny), '315');
        });
        it('correctly reduces numbers to simplified format', () => {
            assert.equal(Utils.simplifyNumber(small), '774k');
            assert.equal(Utils.simplifyNumber(average), '14kk');
            assert.equal(Utils.simplifyNumber(epic), '56kkk');
            assert.equal(Utils.simplifyNumber(string), '51kk');
        });
    });

    describe('iskify',() => {
        it('returns a string', () => {
            assert.typeOf(Utils.iskify(average), 'string');
        });
        it('accepts a number or string',() => {
            assert(Utils.iskify(average));
            assert(Utils.iskify(string));
        });
        it('throws if can;t use argument', () => {
            assert.throws(() => Utils.iskify(unacceptable));
        });
        it('reduces numbers to readable string', () => {
            assert.equal(Utils.iskify(tiny), '315 isk');
            assert.equal(Utils.iskify(small), '774 301 isk');
            assert.equal(Utils.iskify(average), '14 164 332 isk');
            assert.equal(Utils.iskify(epic), '56 400 112 500 isk');
            assert.equal(Utils.iskify(string), '51 230 445 isk');
        });
    });
});