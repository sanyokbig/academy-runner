import {expect} from 'chai';
import mongoose from 'server/mongoose';

import './EveAuth';
import Publication from './Publication';

describe('Publication', () => {
    const defaultConfig = {model: 'modelName', topic: 'topicName'};

    describe('_isWhitelist', () => {
        [
            [{_id: 0, characterName: 1, characterID: 1, groups: 1}, true],
            [{_id: 0, api: 0, groups: 0}, false],
            [{__v: 0}, false]
        ].map(_case => {
            it('identifies ' + JSON.stringify(_case[0]) + ' as ' + (_case[1] ? 'whitelist' : 'blacklist'), () => {
                expect(
                    new Publication({
                        ...defaultConfig, fields: _case[0]
                    })._isWhitelist()
                ).to.equal(_case[1]);
            });
        });
    });

    describe('strip', () => {
        it('have not tests', () => {
            expect(true).to.equal(true);
        });
    });
});

after(done => {
    mongoose.connection.close();
    done();
});