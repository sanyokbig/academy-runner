import moment from 'moment';

import User from 'models/User';
import Token from 'models/Token';
import Authenticator from 'server/modules/Authenticator';
import EveSSO from 'server/modules/EveSSO';
import closePage from 'templates/closePage';
import Logger from 'server/modules/Logger';

const logger = new Logger('AuthCallbackResponder');

export default class AuthCallbackResponder {
    constructor(req, res) {
        this.req = req;
        this.res = res;

        this.respond();
    }

    async respond() {
        const authenticator = new Authenticator(this.req.sessionID),
            eveSSO = new EveSSO(this.req.query.code);

        try {
            // Получение данных по токену и юзеру
            await eveSSO.getTokenData();
            await eveSSO.verify();

            const normalizedTokenData = {
                access_token: eveSSO.tokenData.access_token,
                refresh_token: eveSSO.tokenData.refresh_token,
                expires_at: moment.utc().add(eveSSO.tokenData.expires_in, 'seconds').toDate(),
                scopes: eveSSO.tokenData.Scopes.split(' '),
                type: eveSSO.tokenData.TokenType
            };

            // Поиск или создание юзера
            const user = await User.prepareForLogin(eveSSO.userData.characterID, eveSSO.userData.characterName);

            // Ищем токен юзера
            let token = await user.getToken();

            if (token) {
                // Токен есть - обновляем
                token.set(normalizedTokenData);
            } else {
                // Токена нет - создаем новый и приписываем его к юзеру
                token = await new Token(normalizedTokenData);
                user.token = token._id;
            }

            // Сохранение
            await Promise.all([user.save(), token.save()]);

            // Наконец, авторизация
            await authenticator.authorize(user);

            await authenticator.loginSockets();
        } catch (e) {
            logger.error(e);
            this.res.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
            return this.res.end('Все очень плохо. :(<br><br>' + e.message);
        }

        return this.res.end(closePage());
    }
}