import moment from 'moment';
import request from 'request-promise';
import XML from 'pixl-xml';

import oauth2 from 'server/oauth2';
import Logger from './Logger';

const logger = new Logger('ApiReader');

export default class ApiReader {
    constructor(token) {
        this.token = token; // Инстанс токена
    }

    isExpired() {
        return moment.utc().isSameOrAfter(this.token.expires_at);
    }

    // Обновляет токен и сохраняет
    async refresh(ignoreExpire) {
        if (!ignoreExpire && !this.isExpired()) {
            return this;
        }

        const tokenObject = oauth2.accessToken.create(this.token);

        let result;
        try {
            result = await tokenObject.refresh();
        } catch (e) {
            logger.error(e);
            throw new Error('token refresh failed');
        }

        Object.assign(this.token, result.token);

        try {
            await this.token.save();
        } catch (e) {
            logger.error(e);
            throw new Error('token save failed');
        }

        return this;
    }

    async useESI(endpoint, data = {}) {
        logger.info('Using ESI', endpoint);

        if (!endpoint) {
            logger.error('ESI use aborted: endpoint not specified');
            throw new Error('endpoint not specified');
        }

        data.token = this.token.access_token;

        const options = {
            url: 'https://esi.tech.ccp.is/latest/' + endpoint,
            ...data
        };

        let res;

        try {
            res = await request.get(options);
        } catch (e) {
            logger.error(e.message);
            throw new Error('request error');
        }

        try {
            return JSON.parse(res);
        } catch (e) {
            logger.error(e.message);
            throw new Error('parse error');
        }
    }

    async useXML(path, data = {}) {
        logger.info('Using XML', path, JSON.stringify(data));

        if (!path) {
            logger.alert('XML use aborted: path not specified');
            throw new Error('path not specified');
        }

        data.accessToken = this.token.access_token;

        const options = {
            url: 'https://api.eveonline.com/' + path + '.xml.aspx',
            qs: {
                ...data
            }
        };

        let res;
        try {
            res = await request.get(options);
        } catch (e) {
            logger.error(e);
            throw new Error('request error');
        }

        try {
            return XML.parse(res).result.rowset;
        } catch (e) {
            logger.error(e);
            throw new Error('xml parse error');
        }
    }
}