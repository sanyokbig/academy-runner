import moment from 'moment';

import Logger from 'server/modules/Logger';
import Income from 'models/Income';

const logger = new Logger('TaxProcessor');

export default class TaxProcessor {
    incomes = [];

    async process(taxes, taxRate) {
        this.incomes = taxes.map(tax => ({
            _id: tax.refID,
            characterID: +tax.ownerID2,
            amount: Math.round(tax.amount / taxRate),
            date: moment.utc(tax.date).toDate()
        }));

        let saved = 0,
            exists = 0,
            failed = 0;

        for (let _income of this.incomes) {
            // Если записи в базе нет - работаем
            if (!await Income.getOne({_id: _income._id})) {
                const income = new Income(_income);

                try {
                    await income.save();
                    saved++;
                } catch (e) {
                    logger.alert(e);
                    failed++;
                }
            } else {
                exists++;
            }
        }

        logger.result(
            'Processed', taxes.length, 'taxes:',
            saved, 'saved,',
            exists, 'exists,',
            failed, 'failed,'
        );
    }
}