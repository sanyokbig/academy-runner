import Logger from './Logger';

const logger = new Logger('Connector');

export default class Connector {
    static models = [];
    static action = () => logger.alert('connector stub called, this is wrong');

    // Соединение с моделью через схему
    static connect(model, schema) {
        if (typeof model !== 'string') {
            logger.alert('Model name must be string,', typeof model, 'is passed');
            return;
        }

        if (!schema) {
            logger.alert('No schema passed');
            return;
        }

        logger.info('Connecting with', model);
        // Смотрим, был ли топик подключен ранее
        if (this.models.includes(model)) {
            // Модель подключена, выходим
            return;
        }

        // Записываем в подключенные модели
        this.models.push(model);

        // Подключение пост-хуков
        ['save', 'remove'].map(event => {
            schema.post(event, doc => {
                // Модель изменена
                this.action(event, model, doc);
            });
        });
        logger.result('Model', model, 'connected');
    }
}