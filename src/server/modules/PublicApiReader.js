import request from 'request-promise';
import XML from 'pixl-xml';

import Logger from './Logger';

const logger = new Logger('PublicApiReader');

export default class PublicApiReader {
    async useESI(endpoint, data = {}) {
        logger.info('Using ESI', endpoint);

        if (!endpoint) {
            logger.error('ESI use aborted: endpoint not specified');
            throw new Error('endpoint not specified');
        }

        const options = {
            url: 'https://esi.tech.ccp.is/latest/' + endpoint,
            ...data
        };

        let res;

        try {
            res = await request.get(options);
        } catch (e) {
            logger.error(e.message);
            throw new Error('request error');
        }

        try {
            return JSON.parse(res);
        } catch (e) {
            logger.error(e.message);
            throw new Error('parse error');
        }
    }

    async useXML(path, data = {}) {
        logger.info('Using XML', path, JSON.stringify(data));

        if (!path) {
            logger.alert('XML use aborted: path not specified');
            throw new Error('path not specified');
        }

        const options = {
            url: 'https://api.eveonline.com/' + path + '.xml.aspx',
            qs: {
                ...data
            }
        };

        let res;
        try {
            res = await request.get(options);
        } catch (e) {
            logger.error(e);
            throw new Error('request error');
        }

        try {
            return XML.parse(res).result.rowset;
        } catch (e) {
            logger.error(e);
            throw new Error('xml parse error');
        }
    }
}