import Logger from 'server/modules/Logger';

const logger = new Logger('Draft');

export default class Draft {
    _timeout = 5000;
    _action = null;
    _undone = null;
    _require = null;
    _timerID = null;

    constructor({timeout, action, require, undone}) {
        this._timeout = timeout;
        this._require = require;
        this._action = action;
        this._undone = undone;
    }

    async run() {
        this._timerID = setTimeout(() => this.execute(), this._timeout);
        return this;
    }

    execute() {
        logger.info('Executing draft');
        this._action();
    }

    cancel() {
        logger.result('Draft cancelled');
        clearTimeout(this._timerID);
        this._undone();
    }
}