import request from 'request-promise';
import moment from 'moment';

import Kill from 'models/Kill';
import Logger from 'server/modules/Logger';

const logger = new Logger('zKillboard');

export default class ZKillboard {
    // Инициирует подтягивание киллов за последние переданные часы
    async fetch(hours = 24) {
        logger.info('Fetching kills for', hours, 'hours');

        await Promise.all([
            this._fetch('kills', hours),
            this._fetch('losses', hours)
        ]);

        logger.result('Fetching done');
    }

    // Запрашивает киллы либо лоссы академки, парсит их и записывает в базу
    async _fetch(type, hours = 24) {
        // Проверяем валидность типа
        if (!['kills', 'losses'].includes(type)) {
            logger.alert('\'type\' not passed / invalid, must be \'kills\' or \'losses\'');
            return;
        }

        let page = 1, fetched = [], json, isKill = type === 'kills';

        // Подтягивание всех киллов корпы за заданное время
        do {
            json = await this._readZkbApi(type, 'corporation/98134828', 'no-items', 'page/' + page++, 'pastSeconds/' + 3600 * hours);
            fetched = fetched.concat(json);
        } while (json.length);

        logger.info('Processing', fetched.length, type);

        // Запись киллов в базу, если их там нет
        const processing = [];
        for (let record of fetched) {
            processing.push(this.process(record, isKill));
        }

        const processed = await Promise.all(processing);

        logger.result(
            'Processing', type, 'done:',
            processed.filter(_p => _p === 'added').length, 'added,',
            processed.filter(_p => _p === 'exists').length, 'exists,',
            processed.filter(_p => !_p).length, 'failed'
        );
    }

    // Принимает набор модификаторов в аргументах, делаем по ним запрос, возвращает сырой ответ
    async _readZkbApi(...modifiers) {
        const generatedPath = 'https://zkillboard.com/api/' + modifiers.join('/') + '/';

        try {
            logger.info('Requesting to', generatedPath);
            return JSON.parse(await request.get(generatedPath));
        } catch (e) {
            logger.alert(e.message);
            return [];
        }
    }

    async process(data, isKill){
        let exists = null;

        // Ищем в базе
        try {
            exists = await Kill.findOne({killID: data.killID});
        } catch (e) {
            logger.error(e);
            return false;
        }

        // Если записи нет, генерим и сохраняем
        if (!exists) {
            // Вовлеченные пилоты академии, если лосс, то запись всегда одна
            const involved = isKill
                ? data.attackers.filter(item => item.corporationID === 98134828).map(item => +item.characterID)
                : [+data.victim.characterID];

            const kill = new Kill({
                type: isKill ? 'kill' : 'loss',
                killID: data.killID,
                date: moment.utc(data.killTime),
                totalValue: data.zkb.totalValue,
                points: data.zkb.points,
                npv: data.zkb.npc,
                involved
            });

            try {
                await kill.save();
            } catch (e) {
                logger.error(e);
                return false;
            }
            return 'added';
        }
        // Запись есть
        return 'exists';
    }
}

