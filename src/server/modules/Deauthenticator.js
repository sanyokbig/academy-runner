import Session from 'models/Session';
import Logger from 'server/modules/Logger';
import socketServer from 'server/socketServer';
import {AUTH_LOGOFF_OK} from 'state/auth/actions';

const logger = new Logger('Deauthenticator');

export default class Deauthenticator {
    constructor(sessionID) {
        this.sessionID = sessionID;
    }

    async deauthorize(){
        //Ищем переданую сессию
        this.session = await Session.get(this.sessionID);

        if (!this.session) {
            logger.error('Session not found:', this.sessionID);
            throw new Error('session not found');
        }

        //Сессия найдена, логофаемся
        try {
            await this.session.logoff();
        } catch (e) {
            logger.error(e);
            throw new Error('session logoff failed');
        }

        logger.result('Successfully logged off from session:',this.sessionID);
    }

    logoffSockets() {
        for (let socketId of this.session.session.sockets) {
            socketServer.sockets.to(socketId).emit('action', {type: AUTH_LOGOFF_OK});
        }
    }
}