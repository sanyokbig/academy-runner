import ApiReader from './ApiReader';
import Logger from 'server/modules/Logger';

const logger = new Logger('PilotsReader');

export default class PilotsReader extends ApiReader {
    pilots = [];
    async read() {
        logger.info('Reading pilots');

        await this.refresh();

        try {
            const response = await this.useXML('corp/MemberTracking', {
                accessType: 'corporation',
                extended: true
            });

            this.pilots = response.row;
        } catch (e) {
            logger.error(e.message);
            throw new Error('pilots read failed');
        }

        logger.result('Got', this.pilots.length, 'pilots');

        return this.pilots;
    }
}