import Logger from 'server/modules/Logger';
import Draft from 'server/modules/Draft';

const logger = new Logger('Drafter');

export default class Drafter {
    drafts = {};
    lastId = 0;

    add(config) {
        const draft = new Draft(config);
        this.drafts[++this.lastId] = draft;

        logger.info('draft added', this.lastId);

        draft.run();
        return this.lastId;
    }

    remove(id) {
        const draft = this.drafts[id];

        draft.cancel();
    }

    find(id) {
        return this.drafts[id];
    }

    async mark(doc, draftId, sessionID) {
        if(!doc) {
            logger.alert('cannot be marked: document is', doc);
            return;
        }

        doc.set({
            _draft: {
                id: draftId,
                initiator: sessionID
            }
        });

        try {
            await doc.save();
        } catch (e) {
            logger.error(e);
        }
    }

    async unmark(doc) {
        if(!doc) {
            logger.alert('cannot be unmarked: document is', doc);
            return;
        }

        try {
            doc && doc.set({
                _draft: null
            }) && await doc.save();
        } catch (e) {
            logger.error(e);
        }
    }
}