import config from 'server/config';
import Logger from 'server/modules/Logger';
import Token from 'models/Token';
import PublicApiReader from './PublicApiReader';
import TaxesReader from './TaxesReader';
import TaxProcessor from './TaxProcessor';

const logger = new Logger('Taxman');

export default class Taxman {
    // Получение текущего налога корпы
    async getTaxRate() {
        logger.info('Getting tax rate');
        try {
            return (await new PublicApiReader().useESI('corporations/' + config.academy.corpID)).tax_rate;
        } catch (e) {
            logger.alert('Failed to get tax rate:', e.message);
            throw new Error('useESI failed');
        }
    }


    // Обновление доходов
    async fetch(hours = 24) {
        logger.info('Updating incomes');

        const tokens = await Token.get({scopes: 'corporationWalletRead'});

        for (let token of tokens) {
            const taxesReader = new TaxesReader(token),
                taxProcessor = new TaxProcessor();

            try {
                const [taxes, taxRate] = await Promise.all([
                    taxesReader.read(hours),
                    this.getTaxRate()
                ]);

                await taxProcessor.process(taxes, taxRate);
            } catch (e) {
                logger.error(e.message);
            }
        }
    }
}
