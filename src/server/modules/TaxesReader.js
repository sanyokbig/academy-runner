import moment from 'moment';

import ApiReader from './ApiReader';
import Logger from 'server/modules/Logger';

const logger = new Logger('TaxesReader');

export default class TaxesReader extends ApiReader {
    TAX_REF_TYPE_ID = 85;

    taxes = [];

    async read(hours = 24) {
        logger.info('Reading incomes for', hours, 'hours');

        await this.refresh();

        const lookTill = moment.utc().subtract(hours, 'hours').toDate();

        let fromID = 0,
            journal;

        // Чтение журнала до конца месяца или до нужной даты
        while ((journal = await this._readJournal(fromID)).length) {
            for (let entry of journal) {
                fromID = +entry.refID;

                // Если прошли дальше нужной даты - заканчиваем
                if (moment.utc(entry.date).toDate() < lookTill) {
                    logger.result('Got', this.taxes.length, 'incomes');
                    return this.taxes;
                }

                // Добавление записи о налогах
                if (+entry.refTypeID === this.TAX_REF_TYPE_ID) {
                    this.taxes.push({
                        ...entry,
                        date: moment.utc(entry.date).toDate()
                    });
                }
            }
        }

        logger.result('Got', this.taxes.length, 'incomes');

        return this.taxes;
    }

    async _readJournal(fromID = 0) {
        let journal;
        try {
            journal = await this.useXML('corp/WalletJournal', {
                accessType: 'corporation',
                accountKey: 1000,
                fromID
            });
        } catch (e) {
            logger.alert('Journal read failed:', e.message);
            return [];
        }

        return journal.row;
    }
}