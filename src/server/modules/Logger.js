import chalk from 'chalk';
import fs from 'fs-extra';

class Logger {
    static path = 'server.log';
    // Отложенный лог, будет записан после открытия файла
    static exclude = [];
    static deferred = '';
    static lastDate = null;
    // Индикатор процесса открытия файла
    static fsWIP = false;
    // Идентификатор файла логов
    static fd = null;

    // Открытие файла
    static openLog() {
        fs
            .open(Logger.path, 'w')
            .then(fd => {
                this.fd = fd;
                this.fsWIP = false;
                // Запись отложенного лога
                fs.write(this.fd, this.deferred);
            });
    }

    constructor(module) {
        if (process.env.DISABLE_LOGGER) {
            this.log = this.info = this.result = this.alert = this.error = Logger.openLog = () => null;
        }

        // Префикс для логов
        this.module = module;

        // Обнуление исключенных функций
        Logger.exclude.map(method => this[method] = () => null);

        // Начать открытие лога, если файл не открыт и открытие не в процессе
        (!Logger.fsWIP && !Logger.fd) && Logger.openLog();
    }

    log(...message) {
        this._output('log', 'white', 'L', message);
    }

    info(...message) {
        this._output('log', 'blue', 'I', message);
    }

    result(...message) {
        this._output('log', 'green', 'R', message);
    }

    alert(...message) {
        this._output('log', 'yellow', 'A', message);
    }

    error(...message) {
        this._output('log', 'red', 'E', message);
    }

    // Обновление даты
    _update() {
        if (!Logger.lastDate || new Date().toUTCString() !== Logger.lastDate.toUTCString()) {
            const newDate = new Date();
            Logger.lastDate = newDate;
            return '[' + newDate.toUTCString() + ']';
        }
        return null;
    }

    // Вывод сообщения
    _output(method, color, type, message) {
        const newDate = this._update();

        if (newDate) {
            this._writeDate(newDate);
            console[method](chalk.bold.blue(newDate));
        }

        const module = '[' + this.module + ']';
        type = '[' + type + ']';

        this._write(type, module, message.join(' '));

        console[method](
            '\t',
            chalk[color](module),
            ...message
        );
    }

    // Запись новой даты в файл
    _writeDate(date) {
        date += '\n';
        Logger.fd
            ? fs.write(Logger.fd, date)
            : Logger.deferred += date;
    }

    // Запись сообщения
    _write(...message) {
        message = '\t' + message.join(' ') + '\n';
        Logger.fd
            ? fs.write(Logger.fd, message)
            : Logger.deferred += message;
    }
}

export default Logger;