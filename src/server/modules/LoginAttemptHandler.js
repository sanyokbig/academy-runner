import randToken from 'rand-token';

import Session from 'models/Session';
import oauth2 from 'server/oauth2';
import Logger from 'server/modules/Logger';
import config from 'server/config';

const logger = new Logger('LoginAttemptHandler');

export default class LoginAttemptHandler {
    constructor(sessionID) {
        this.sessionID = sessionID;
    }

    async prepareSession({scopes}) {
        logger.info('Preparing session', this.sessionID, 'for login');
        //Генерация стейта для передачи на клиент и сопоставления при ответе с сервера EVE
        const state = randToken.generate(128);

        // Генерация адреса авторизации
        const authorizationUri = oauth2.authorizationCode.authorizeURL({
            redirect_uri: 'http://' + config.server.host + ':' + config.server.port + '/_eveauth/callback',
            scope: scopes && scopes.length ? scopes.join(' ') : '',
            state: state
        });
        //Запись стейта в сессию
        const session = await Session.get(this.sessionID);

        if (!session) {
            //Сессия не найдена, логин не прошел
            logger.error('Session not found', this.sessionID);
            throw new Error('session not found');
        }

        //Сессия найдена, записываем в нее юзера
        session.session.state = state;

        try {
            await session.save();
        } catch (e) {
            logger.error(e);
            throw new Error('mongo error');
        }

        logger.result('Session', this.sessionID, 'prepared with state', state);
        return authorizationUri;
    }
}