import Session from 'models/Session';
import {NOT_LOGGED_IN} from 'server/constants';

export default class AuthStatePasser {
    constructor(...args) {
        this.pass(...args);
    }

    async pass(req, res, next) {
        const session = await Session.get(req.sessionID);
        req.authState = session
            ? await session.getAuthState()
            : NOT_LOGGED_IN;

        next();
    }
}