import sift from 'sift';

import socketServer from 'server/socketServer';
import mongoose from 'server/mongoose';
import Session from 'models/Session';
import Logger from 'server/modules/Logger';

import {SUBS_REMOVED} from 'state/subs/actions';

const logger = new Logger('Publication');

export default class Publication {
    constructor(config = {}) {
        const {
            topic, model, query, prevQuery,
            fields, require, uniqueField, transform, populate
        } = config;

        if (!topic) throw new Error('Topic is missing');
        if (!model) throw new Error('Model is missing');

        this.topic = topic;
        this.model = model;

        this._query = query || {};
        this._prevQuery = prevQuery || null;
        this._fields = fields || {};
        this._require = require || null;
        this._uniqueField = uniqueField || '_id';

        this._transform = transform || (doc => doc);
        this._populate = populate || null;

        this._subscribers = [];
    }

    // Добавление подписчика
    addSubscriber = async (sessionID) => {
        if (!await this.canAccess(sessionID)) {
            logger.alert('Can\'t add subscriber', sessionID);
            return false;
        }

        // Сессия существует, если не подписана - подписываем на топик и отправляем все текущие данные
        if (this._subscribers.includes(sessionID)) {
            logger.result('Session', sessionID, 'already subscribed to', this.topic);
            return true;
        }

        // Подписываем
        this._subscribers.push(sessionID);
        logger.result('Session', sessionID, 'subscribed to', this.topic);
        return true;
    };

    // Удаление подписчика
    removeSubscriber = (sessionID) => {
        // Удаляем подписку
        this._subscribers = this._subscribers.filter(_sub => _sub !== sessionID);
    };

    // Определяет, является ли список белым
    _isWhitelist() {
        for (let field in this._fields)
            if (this._fields.hasOwnProperty(field) && field !== '_id')
                return !!this._fields[field];

        return false;
    }

    // Убирает лишние поля и по необходимости трансформирует значения
    _prepareDocument = async doc => {
        this._populate && (doc = await doc.populate(this._populate).execPopulate());

        doc = doc.toObject();

        doc = this._strip(doc);
        doc = this._transform(doc);

        return doc;
    };

    // Возвращает копию переданного объекта c полями, определенными вторым объектом
    _strip(object) {
        // Определение типа списка и подготовка объекта
        const whitelist = this._isWhitelist(this._fields),
            stripped = {};

        if (!this._fields.hasOwnProperty('_id') || this._fields._id !== 0) {
            stripped._id = object._id;
        }

        // Наполнение объекта полями
        for (let objectField in object) {
            if (object.hasOwnProperty(objectField)) {
                if (objectField !== '_id' && this._fields.hasOwnProperty(objectField) === whitelist) {
                    stripped[objectField] = object[objectField];
                }
            }
        }

        return stripped;
    }

    // Получение текущих документов публикации
    async getCurrent(sessionID) {
        if (!sessionID) throw new Error('sessionID required');

        let current;

        const query = mongoose.model(this.model)
            .find({
                ...this._query,
                // Либо драфта нет, либо сессия является инициатором драфта
                $or: [{
                    _draft: null
                }, {
                    '_draft.initiator': sessionID
                }]
            })
            .select(this._fields);

        this._populate && query.populate(this._populate);

        try {
            current = await query.exec();
        } catch (e) {
            logger.error(e);
            return [];
        }

        return await Promise.all(current.map(async doc => await this._prepareDocument(doc)));
    }

    // Проверяет доступ сессии к топику
    async canAccess(sessionID) {
        const session = await Session.get(sessionID);
        // Если роль требуется, проверяем, иначе тру
        return this._require && session && session.haveAccess(this._require);
    }

    // Обработчик изменения документа
    documentChanged = async (doc, type) => {
        let removed;
        // Проверка документы на соответствие публикации
        if (!sift(this._query)(doc)) {
            // Текущей публикации документ не подходит, проверям прошлое состояние
            if (this._prevQuery && sift(this._prevQuery)(doc)) {
                // Обнуление _prevQuery, иначе каждое изменение документа может распознаваться как изменение видимости
                await mongoose.model(this.model).resetPrevQuery(doc._id);
                // С прошлым состоянием все норм, документ вышел из видимости, шлем удаление
                removed = true;
            } else {
                // Тупик, не подходит, пропускаем
                return;
            }
        }

        this._subscribers.map(sessionID => {
            let _type;
            // Обработка драфта записи
            if (doc._draft) {
                // Является ли подписчик инициатором драфта?
                _type = sift({['_draft.initiator']: sessionID})(doc)
                    ? type // Является, обновляем запись для возможности отмены
                    : SUBS_REMOVED; // Не явзяелся, запись "удаляется"
            } else {
                // Запись не в драфте, стандартная обработка
                _type = removed
                    ? SUBS_REMOVED
                    : type;
            }

            // Получаем сокеты сессии, эмитим сообщение каждому
            Session
                .getSockets(sessionID)
                .then(sockets =>
                    sockets.map(async socket =>
                        this.emit(socket, _type, {
                            topic: this.topic,
                            doc: await this._prepareDocument(doc),
                            uniqueField: this._uniqueField
                        })));
        });
    };

    // Отправка сообщения на фронт
    emit(socket, type, data) {
        socketServer.sockets.to(socket).emit('action', {type, data});
    }
}