import socketServer from 'server/socketServer';
import Logger from 'server/modules/Logger';
import Session from 'models/Session';
import Publication from './Publication';
import Connector from './Connector';
import RolesRefresher from './RolesRefresher';

import {SUBS_UPDATED, SUBS_REMOVED, SUBS_CURRENT, SUBS_UNSUBSCRIBE} from 'state/subs/actions';
import {AUTH_ROLES} from 'state/auth/actions';
import sift from 'sift';

const logger = new Logger('Publisher');

// Публикация и подпись на топики
export default class Publisher {
    constructor() {
        this.connector = Connector;
        this.connector.action = this.onPost.bind(this);

        // Список публикаций
        this.publications = [];
    }

    // Публикация топика
    publish(topic, model, config = {}) {
        const query = config.query || {},
            prevQuery = config.prevQuery || null,
            fields = config.fields || {},
            require = config.require || null,
            uniqueField = config.uniqueField || '_id',
            transform = config.transform || null,
            populate = config.populate || null;

        if (!topic || !model) {
            logger.alert('Can\'t publish: ', !topic ? 'topic' : 'model', 'is missing');
            return this;
        }

        // Подключена ли модель?
        if (!this.connector.models.includes(model)) {
            logger.alert('Can\'t publish: model', model, 'is not connected');
            return this;
        }

        // Проверка на существование публикации
        if (this.findPublication(topic)) {
            logger.alert('Can\'t publish: publication', topic, 'already exists');
            return this;
        }

        // Создаем публикацию
        this.publications.push(new Publication({
            topic,
            model,
            query,
            prevQuery,
            fields,
            require,
            uniqueField,
            transform,
            populate
        }));

        logger.result('Successfully published topic', topic);
        return this;
    }

    // Подпись клиента на топик
    async subscribeToTopic(sessionID, topic) {
        logger.info('Subscribing', sessionID, 'to topic', topic);

        // Ищем публикацию топика
        const pub = this.findPublication(topic);
        if (!pub) {
            logger.alert('Can\'t subscribe to', topic, ': publication not found');
            return false;
        }

        pub.addSubscriber(sessionID);

        return pub;
    }

    // Подписка успешна, передаем клиенту все текущие записи
    async onSubscription(sessionID, topic) {
        const pub = this.findPublication(topic);
        // Документы публкикации
        setTimeout(async () => {
            const docs = await pub.getCurrent(sessionID);
            // Находим все сокеты сессии
            Session
                .getSockets(sessionID)
                .then(sockets => {
                    // Эмитим запись на каждый сокет
                    sockets.map(socket => this.emit(socket, SUBS_CURRENT, {topic, docs}));
                });
        }, 200);
    }

    // Изменение документа, сообщаем всем подписчикам топика
    async onPost(event, model, doc) {
        const type = event === 'save' ? SUBS_UPDATED : SUBS_REMOVED;

        // Если изменились права доступа
        if (doc._authRulesChanged) {
            // Обновляем роля в сессиях, получаем обновленные сессии
            const sessions = await new RolesRefresher(model, doc).refresh();

            sessions.map(session => {
                this.stripInaccessible(session._id);
                session.session.sockets.map(socket =>
                    this.emit(socket, AUTH_ROLES, {roles: session.session.roles})
                );
            });
        }

        // Список публикаций модели
        const pubs = this.findPublicationsByModel(model);
        // Отправка изменений подписчикам соответствующих публикаций
        pubs.map(pub => pub.documentChanged(doc, type));
    }

    // Отправка сообщения на фронт
    emit(socket, type, data) {
        socketServer.sockets.to(socket).emit('action', {type, data});
    }

    // Поиск публикации по топику
    findPublication(topic) {
        return this.publications.find(_pub => _pub.topic === topic);
    }


    // Поиск публикациЙ по модели
    findPublicationsByModel(model) {
        return this.publications.filter(_pub => _pub.model === model);
    }

    // Поиск публикаций по сессии
    findPublicationsBySession(sessionID) {
        return sift({_subscribers: sessionID}, this.publications);
    }

    // Отписка сессии от топика
    async unsubscribe(sessionID, topic) {
        const pub = this.findPublication(topic);

        pub.removeSubscriber(sessionID);

        // Шлем сообщение об отписке
        const sockets = await Session.getSockets(sessionID);

        // Эмитим отписку на каждый сокет
        sockets.map(socket => this.emit(socket, SUBS_UNSUBSCRIBE, {topic}));
    }

    // Лишает сессию подписок на более недоступные публикации
    async stripInaccessible(sessionID) {
        const pubs = this.findPublicationsBySession(sessionID);
        for (let pub of pubs) {
            !await pub.canAccess(sessionID) && this.unsubscribe(sessionID, pub.topic);
        }
    }

    // Серверная подписка на топики, возвращает данные по топику
    async serverSideSubscription(sessionID, topics = []) {
        const data = {},
            promises = topics.map(async topic => {
                const pub = this.findPublication(topic);

                // Проверка доступа к публикации
                if (await pub.canAccess(sessionID)) {
                    // Подписываемся на публикацию и тянем текущие данные
                    if (await pub.addSubscriber(sessionID)) {
                        data[topic] = await pub.getCurrent(sessionID);
                    }
                }
            });

        await Promise.all(promises);

        return data;
    }
}

