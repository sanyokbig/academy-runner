import Logger from 'server/modules/Logger';
import Session from 'models/Session';

import {
    NTF_ADD
} from 'state/notifications/actions';

const logger = new Logger('SocketGuardian');

export default class SocketGuardian {
    constructor(socket) {
        this.socket = socket;
    }

    // Проверяет наличие роли у сокета, если роли нет - возвращает false и отсылает сокет сообщение на фронт с ошибкой доступа
    async verifyAccess(role, silent = false) {
        const sessionID = this.socket.handshake.sessionID;
        logger.info('Verifying access for session', sessionID, 'of role', role);

        const session = await Session.get(sessionID);
        const access = session && await session.haveAccess(role);

        // Доступа нет, или ошибка сессии - отсылаем сообщение
        if (access) {
            logger.result('Access verified for', sessionID);
        } else {
            logger.result('Access denied for', sessionID);
            !silent && this.socket.emit('action', {
                type: NTF_ADD,
                ntf: {
                    value: 'server.insufficientRoles'
                }
            });
        }

        return access;
    }
}