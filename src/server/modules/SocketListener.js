import Session from 'models/Session';
import methods from 'server/methods/index';
import Logger from 'server/modules/Logger';
import config from 'server/config';

import {AUTH_LOGOFF, AUTH_LOGIN} from 'state/auth/actions';
import {GROUPS_ADD_ROLE, GROUPS_REMOVE_ROLE} from 'state/groups/actions';
import {SUBS_SUBSCRIBE} from 'state/subs/actions';
import {USERS_ADD_TO_GROUP, USERS_REMOVE_FROM_GROUP, USERS_REMOVE} from 'state/users/actions';
import {DRAFT_CANCEL} from 'state/draft/actions';

const logger = new Logger('SocketListener');

export default class SocketListener {
    constructor(socket) {
        this.socket = socket;
        this.connected();
        this.socket.on('action', action => {
            this.listen(action);
        });
    }

    async connected() {
        logger.info('Socket connected:', this.socket.id);

        const socketId = this.socket.id,
            sessionID = this.socket.handshake.sessionID,
            session = await Session.get(sessionID);

        // Сессия не найдена
        if (!session) {
            logger.alert('Session', sessionID, 'not found');
            return;
        }

        //Сессия найдена
        session.session.sockets || (session.session.sockets = []);

        //Если достигнут лимит сокетов на сессию, выпиливаем самый старый
        if (session.session.sockets.length > config.session.maxSockets) {
            session.session.sockets.shift();
            // TODO оповещать сокет о выпиле
        }

        //Добавляем сокет
        session.session.sockets.push(socketId);

        try {
            await session.save();
        } catch (e) {
            logger.error(e);
        }
    }

    async disconnected(socket) {
        logger.info('Socket disconnected:', socket.id);

        const sessionID = socket.handshake.sessionID,
            socketId = socket.id,
            session = await Session.get(sessionID);

        if (!session) {
            logger.alert('Session', sessionID, 'not found');
            return;
        }

        //Сессия найдена, удаляем сокет
        session.session.sockets.filter(socket => socket !== socketId);

        try {
            await session.save();
        } catch (e) {
            logger.error(e);
        }
    }

    listen(action) {
        const socket = this.socket;
        switch (action.type) {
            case AUTH_LOGIN:
                methods.auth.login(socket, action.data);
                break;
            case AUTH_LOGOFF:
                methods.auth.logoff(socket);
                break;
            case SUBS_SUBSCRIBE:
                methods.subs.subscribe(socket, action.data);
                break;
            case USERS_ADD_TO_GROUP:
                methods.users.addToGroup(socket, action.data);
                break;
            case USERS_REMOVE_FROM_GROUP:
                methods.users.removeFromGroup(socket, action.data);
                break;
            case USERS_REMOVE:
                methods.users.removeUser(socket, action.data);
                break;
            case GROUPS_ADD_ROLE:
                methods.groups.addRole(socket, action.data);
                break;
            case GROUPS_REMOVE_ROLE:
                methods.groups.removeRole(socket, action.data);
                break;
            case DRAFT_CANCEL:
                methods.draft.cancelDraft(socket, action.data);
                break;
            default:
                logger.alert('Unhandled method', action.type);
                break;
        }
    }
}