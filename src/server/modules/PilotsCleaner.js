import Logger from 'server/modules/Logger';
import User from 'models/User';

const logger = new Logger('PlotsCleaner');

export default class PilotsCleaner {
    // Поиск в базе пилотов, если он больше в корпе - менять группу на бывший пилот
    async clean(currentPilots) {
        logger.info('Cleaning pilots');
        const cursor = User.find({groups: 'pilot'}).cursor();
        let removed = 0, failed = 0;
        await cursor.eachAsync(async pilot => {
            if (!currentPilots.includes(pilot.characterID)) {
                // Вне академии, убираем из пилотов, вписываем в бывших пилоты
                pilot.groups = pilot.groups.filter(group => group !== 'pilot');
                pilot.groups.push('expilot');

                try {
                    await pilot.save();
                    removed++;
                } catch (e) {
                    failed++;
                    logger.error(e);
                }
            }
        });
        logger.result('Cleaning done:', removed, 'removed,', failed, 'failed');
    }
}