import Logger from 'server/modules/Logger';
import LoginAttemptHandler from 'server/modules/LoginAttemptHandler';
import Deauthenticator from 'server/modules/Deauthenticator';
import publisher from 'server/publisher';

import {AUTH_LOGOFF_FAIL} from 'state/auth/actions';

const logger = new Logger('methods/auth');

// Запрос на логин, обрабатываем
async function login(socket, data) {
    const loginAttemptHandler = new LoginAttemptHandler(socket.handshake.sessionID);

    try {
        socket.emit('startLogin', {uri: await loginAttemptHandler.prepareSession(data)});
    } catch (e) {
        logger.error(e);
        socket.emit('action', {
            ntf: {
                value: 'server.error'
            }
        });
    }
}

async function logoff(socket) {
    const deauthenticator = new Deauthenticator(socket.handshake.sessionID);

    try {
        await deauthenticator.deauthorize();
    } catch (e) {
        logger.error(e);
        socket.emit('action', {
            type: AUTH_LOGOFF_FAIL,
            ntf: {
                value: 'auth.logoffFailure'
            }
        });
    }

    deauthenticator.logoffSockets();
    publisher.stripInaccessible(deauthenticator.sessionID);
}

export const auth = {
    login,
    logoff
};