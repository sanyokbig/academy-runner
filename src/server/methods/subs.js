import publisher from 'server/publisher';
import {SUBS_SUBSCRIBE_OK, SUBS_SUBSCRIBE_FAIL} from 'state/subs/actions';

// Запрос подлписки
const subscribe = async (socket, data) => {
    const topic = data.topic,
        sessionID = socket.handshake.sessionID,
        emitData = {};

    // Пробуем подписаться
    const sub = await publisher.subscribeToTopic(sessionID, topic);

    if (sub) {
        // Успех, отправляем сообщение и данные на фронт
        publisher.onSubscription(sessionID, topic);

        emitData.type = SUBS_SUBSCRIBE_OK;
        emitData.data = {topic};
    } else {
        // Провал, так же сообщаем
        emitData.type = SUBS_SUBSCRIBE_FAIL;
        emitData.ntf = {
            value: 'server.error'
        };
    }

    socket.emit('action', emitData);

};

export const subs = {
    subscribe
};