import SocketGuardian from 'server/modules/SocketGuardian';
import Logger from 'server/modules/Logger';
import drafter from 'server/drafter';

const logger = new Logger('methods/draft');

async function cancelDraft(socket, {draftId}) {
    const draft = drafter.find(draftId);

    if (!draft) {
        logger.error('draft', draftId, 'not found');
        throw new Error('draft not found');
    }

    if (draft.require && !await new SocketGuardian(socket).verifyAccess('groupsEdit')) return;

    draft.cancel();
}

export const draft = {
    cancelDraft
};