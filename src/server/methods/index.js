import {auth} from './auth';
import {users} from './users';
import {groups} from './groups';
import {subs} from './subs';
import {draft} from './draft';

const methods =  {
    auth,
    users,
    groups,
    subs,
    draft
};

export default methods;