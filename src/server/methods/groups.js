import Group from 'models/Group';
import Logger from 'server/modules/Logger';
import SocketGuardian from 'server/modules/SocketGuardian';

const logger = new Logger('GroupsMethods');

import {
    GROUPS_ADD_ROLE_FAIL,
    GROUPS_REMOVE_ROLE_FAIL
} from 'state/groups/actions';

async function addRole(socket, data) {
    if (!await new SocketGuardian(socket).verifyAccess('groupsEdit')) return;

    const group = await Group.getOne({_id: data.groupID});

    try {
        await group.addRole(data.role);
    } catch (e) {
        logger.alert(e.message);
        socket.emit('action', {
            type: GROUPS_ADD_ROLE_FAIL,
            ntf: {
                value: 'server.error'
            }
        });
    }
}

async function removeRole(socket, data) {
    if (!await new SocketGuardian(socket).verifyAccess('groupsEdit')) return;

    const group = await Group.getOne({_id: data.groupID});

    try {
        await group.removeRole(data.role);
    } catch (e) {
        logger.alert(e.message);
        socket.emit('action', {
            type: GROUPS_REMOVE_ROLE_FAIL,
            ntf: {
                value: 'server.error'
            }
        });
    }
}

export const groups = {
    addRole,
    removeRole
};