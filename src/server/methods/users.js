import User from 'models/User';
import Logger from 'server/modules/Logger';
import SocketGuardian from 'server/modules/SocketGuardian';
import drafter from 'server/drafter';

import {NTF_ADD} from 'state/notifications/actions';

const logger = new Logger('methods/user');

async function addToGroup(socket, data) {
    if (!await new SocketGuardian(socket).verifyAccess('usersEdit')) return;

    const user = await User.getOne({characterID: data.characterID});

    try {
        user && await user.addToGroup(data.groupID);
    } catch (e) {
        logger.error(e);
        socket.emit('action', {
            type: NTF_ADD,
            ntf: {
                value: 'server.error'
            }
        });
    }
}

async function removeFromGroup(socket, data) {
    if (!await new SocketGuardian(socket).verifyAccess('usersEdit')) return;

    const user = await User.getOne({characterID: data.characterID});

    try {
        user && await user.removeFromGroup(data.groupID);
    } catch (e) {
        logger.error(e);
        socket.emit('action', {
            type: NTF_ADD,
            ntf: {
                value: 'server.error'
            }
        });
    }
}

async function removeUser(socket, data) {
    if (!await new SocketGuardian(socket).verifyAccess('usersEdit')) return;
    // Созданифе драфта
    const draftId = drafter.add({
        timeout: 8000,
        require: 'usersEdit',
        action: async () => {
            const user = await User.getOne(data);

            try {
                user && await user.removeUser();
            } catch (e) {
                logger.error(e);
                socket.emit('action', {
                    type: NTF_ADD,
                    ntf: {
                        value: 'server.error'
                    }
                });
            }
        },
        undone: async () => {
            const user = await User.getOne(data);
            await drafter.unmark(user);
        }
    });

    // Пометка юзера
    const user = await User.getOne({characterID: data.characterID});
    await drafter.mark(user, draftId, socket.handshake.sessionID);
}

export const users = {
    addToGroup,
    removeFromGroup,
    removeUser
};