import moment from 'moment';

moment.updateLocale('ru', {
    relativeTime: {
        past: '%s'
    }
});