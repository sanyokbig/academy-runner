import Scheduler from './modules/Scheduler';
import config from './config';
import ZKillboard from './modules/ZKillboard';
import PilotsUpdater from './modules/PilotsUpdater';
import PvpStats from './modules/PvpStats';
import Taxman from './modules/Taxman';
import IncomeStats from './modules/IncomeStats';

new Scheduler()
    .add('pilotsUpdate', config.schedule.pilotsUpdate, async () => {
        await new PilotsUpdater().run();
    })
    .add('zKillboardFetch', config.schedule.zKillboardFetch, async () => {
        // Каждый час тянуть киллы за последние 4 часа
        await new ZKillboard().fetch(4);
        // Пересчитывать киллы за последний месяц
        await new PvpStats().update(30);
    })
    .add('incomeFetch', config.schedule.incomesFetch, async () => {
        // Подтягивание доходов за час каждые полчаса
        await new Taxman().fetch(1);
        // Обновление дохода пилотов за месяц
        await new IncomeStats().update(30);
    });