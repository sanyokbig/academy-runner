import SocketIO from 'socket.io';

const socketServer = new SocketIO();

export default socketServer;