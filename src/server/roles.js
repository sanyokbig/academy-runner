export default [
    'usersRead',
    'usersEdit',
    'groupsRead',
    'groupsEdit',
    'eveApiReader',
    'pilotsRead',
    'incomesRead'
];