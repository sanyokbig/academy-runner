import Bluebird from 'bluebird';
import mongoose from 'mongoose';
import config from '../server/config';

mongoose.Promise = Bluebird;

const options = {
    useMongoClient: true
};

config.mongo.user && (options.user = config.mongo.user);
config.mongo.pass && (options.pass = config.mongo.pass);

mongoose.connect('mongodb://' + config.mongo.server+ '/' +config.mongo.database, options);

export default mongoose;