export function populate(target, source, field = '_id') {
    let initialValue;
    if (Array.isArray(target)) {
        initialValue = [...target];
    } else {
        initialValue = [target];
    }

    let populated = [], found;

    for (let init of initialValue) {
        (found = source.find(v => v[field] === init)) && populated.push(found);
    }

    return populated;
}

// Монгообразный поиск по массиву
export function getFromList(list, query) {
    let valid, queryValue, itemValue, key;
    return list.filter((item) => {
        //Восстанавливаем значение
        valid = true;
        // Проход по всем ключам запроса и сравнимаем значения с итемом
        for (key in query) {
            // Для удобства
            queryValue = query[key];
            itemValue = item[key];

            // ПРоверяем наличие поля в итеме, если нет - выходим
            if (typeof itemValue === 'undefined') return false;

            // Смотрим, массив ли проверяемое значение
            if (Array.isArray(itemValue)) {
                // Массив, если элемент один, вынимаем его и сравниваем, иначе используем includes
                valid &= itemValue.length === 1 ? itemValue[0] === queryValue : itemValue.includes(queryValue);
            } else {
                // Не массив, сравниваем
                valid &= itemValue === queryValue;
            }
        }
        return valid;
    });
}