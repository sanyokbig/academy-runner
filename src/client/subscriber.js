import store from './store';
import {subscribeAction} from '../state/subs/actions';

// Попытка подписать клиент на топик
export const subscribe = (topic) => {
    store.getState().subs.hasOwnProperty(topic) || store.dispatch(subscribeAction(topic));
};