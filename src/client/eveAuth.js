class EveAuth {
    prepareLogin() {
        const width = 800,
            height = 600,
            x = screen.width / 2 - width / 2,
            y = screen.height / 2 - height / 2;

        const settings = 'menubar=0,width=' + width + ',height=' + height + ',left=' + x + ',top=' + y;

        this.authWindow = window.open('', 'eveAuth', settings);
    }

    startLogin(uri) {
        this.authWindow.location.replace(uri);
    }
}

export default new EveAuth();