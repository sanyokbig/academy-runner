import io from 'socket.io-client';
import eveAuthClient from './eveAuth';

const config = process.env.BROWSER ? require('../client/config') : require('../server/config');

const socket = io('http://' + config.server.host + ':' + config.server.port, {});

socket.on('startLogin', data => {
    eveAuthClient.startLogin(data.uri);
});

export default socket;