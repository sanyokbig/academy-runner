import {users, pilots, groups, incomes} from './server/preloaders';

const routesData = [{
    path: '/',
    exact: true,
}, {
    path: '/pilots',
    exact: true,
    loadData: pilots
}, {
    path: '/incomes',
    exact: true,
    loadData: incomes
}, {
    path: '/users',
    exact: true,
    loadData: users
}, {
    path: '/groups',
    exact: true,
    loadData: groups
}];

export default routesData;