const config = process.env.BROWSER ? require('../client/config') : require('../server/config');

class AssetManager {
    static assetUrl = process.env.NODE_ENV !== 'production'
        ? 'http://' + config.server.host + ':' + (((+config.server.port) + 1) || 3001) + '/'
        : '';

    static assets = (process.env.BROWSER)
        ? window.__ASSETS__
        : process.env.PROD
            ? JSON.parse(require('fs').readFileSync(require('path').resolve(__dirname, 'client/manifest.json'), 'utf-8'))
            : JSON.parse(require('fs').readFileSync(require('path').resolve(__dirname, '../../manifest.json'), 'utf-8'));


    static portrait(id) {
        return ('http://image.eveonline.com/Character/' + id + '_32.jpg');
    }

    static ship(id) {
        return ('http://image.eveonline.com/Render/' + id + '_32.png');
    }

    static get(filename) {
        return (this.assetUrl + 'dist/' + (this.assets[filename] || filename));
    }

    static image(filename) {
        return this.get('images/' + filename);
    }

    static zkb(id) {
        return ('https://zkillboard.com/character/' + id);
    }
}

export default AssetManager;