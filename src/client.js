import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

import './client/config';
import App from './ui/App';
import {AppContainer} from 'react-hot-loader';
import store from './client/store';
import './static/favicon.ico';

window && (window.Perf = require('react-addons-perf'));

const render = Component =>
    ReactDOM.render(<AppContainer>
        <Provider store={store}>
            <BrowserRouter>
                <Component/>
            </BrowserRouter>
        </Provider>
    </AppContainer>, document.getElementById('react-view'));


render(App);

if (module.hot) {
    module.hot.accept('./ui/App', () => render(require('./ui/App').default));
}