require('babel-core/register');
['.css', '.less', '.sass', '.ttf', '.woff', '.woff2', '.png', '.jpg', '.jpeg', '.gif', '.styl'].forEach((ext) => require.extensions[ext] = () => {});
require('babel-polyfill');
require('server.js');