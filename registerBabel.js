require('babel-polyfill');
var fs = require('fs'),
    babelrc = fs.readFileSync('./.babelrc'),
    config;

try {
    config = JSON.parse(babelrc);
} catch(e){
    console.error('==>     ERROR: Error parsing your .babelrc.');
    console.error(e);
}

require('babel-register')(config);

['.css', '.less', '.sass', '.ttf', '.woff', '.woff2', '.png', '.jpg', '.jpeg', '.gif', '.styl'].forEach((ext) => require.extensions[ext] = () => {});